package com.example.felipe.gimnasioapp.ui.chat;

import com.example.felipe.gimnasioapp.data.model.ItemChat;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

import java.util.List;

public interface ChatMvpPresenter<V extends ChatMvpView, I extends ChatMvpInteractor>
        extends MvpPresenter<V, I> {

    void onSendMessageClick(ItemChat item);

    void onViewPrepared();

    void onItemAdded(ItemChat item);

}
