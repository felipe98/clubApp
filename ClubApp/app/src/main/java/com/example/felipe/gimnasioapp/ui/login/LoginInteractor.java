package com.example.felipe.gimnasioapp.ui.login;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.BaseInteractor;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by Felipe on 14/07/2018.
 */

public class LoginInteractor extends BaseInteractor implements LoginMvpInteractor {

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private Realm realm;
    private User user;

    @Inject
    public LoginInteractor() {
    }

    @Override
    public void loginUser(final String email, String pass, @NonNull final LoginFirebaseListener listener) {

        realm = Realm.getDefaultInstance();

        database = FirebaseDatabase.getInstance();

        mAuth = FirebaseAuth.getInstance();

        reference = database.getReference("user");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    user = childSnapshot.getValue(User.class);
                    if (user != null) {
                        if (user.getEmail().equals(email)) {
                            addTypeUser(new TypeUser(user.getType()));
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        mAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                listener.loginUser(task.isSuccessful());
            }
        });

    }


    private void addTypeUser(TypeUser item) {
        realm.beginTransaction();
        realm.copyToRealm(item);
        realm.commitTransaction();
    }
}
