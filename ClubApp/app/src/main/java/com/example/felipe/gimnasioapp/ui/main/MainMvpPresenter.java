package com.example.felipe.gimnasioapp.ui.main;

import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

/**
 * Created by Felipe on 12/07/2018.
 */

public interface MainMvpPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends MvpPresenter<V, I> {

    void onDrawerOptionAboutClick();

    void onDrawerChatClick();

    void onDrawerOptionLogoutClick();

    void onDrawerFavoritClick();

    void onDrawerOrderClick();

    void onDrawerAddressClick();

    void onDrawerEditClick();

    void onDrawerAddMenuClick();

    void onDrawerRateUsClick();

    void onDrawerRecentClick();

    void onRestaurantClick();

    void onViewInitialized();

}
