package com.example.felipe.gimnasioapp.ui.main;

import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.ui.base.MvpInteractor;

/**
 * Created by Felipe on 12/07/2018.
 */

public interface MainMvpInteractor extends MvpInteractor {

    String getCurrentUserName();

    String getCurrentUserEmail();

    String getCurrentUserProfilePicUrl();

    String getTypeUser();

    void clearTypeUser();

    void signOff();
}
