package com.example.felipe.gimnasioapp.ui.register;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

/**
 * Created by Felipe on 15/07/2018.
 */

public interface RegisterMvpPresenter<V extends RegisterMvpView, I extends RegisterMvpInteractor>
        extends MvpPresenter<V, I> {

    void onPageChanged(int check);

    void onCheckChanged();
}
