package com.example.felipe.gimnasioapp.ui.register;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.felipe.gimnasioapp.ui.register.restaurant.RestaurantRFragment;
import com.example.felipe.gimnasioapp.ui.register.user.UserRFragment;

/**
 * Created by Felipe on 15/07/2018.
 */

public class RegisterPagerAdapter extends FragmentStatePagerAdapter {

    private static int int_items = 2;

    RegisterPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new UserRFragment();
            case 1:
                return new RestaurantRFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return int_items;
    }
}

