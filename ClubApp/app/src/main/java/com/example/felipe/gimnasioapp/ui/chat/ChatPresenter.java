package com.example.felipe.gimnasioapp.ui.chat;

import android.text.TextUtils;

import com.example.felipe.gimnasioapp.data.model.ItemChat;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class ChatPresenter<V extends ChatMvpView, I extends ChatMvpInteractor>
        extends BasePresenter<V, I> implements ChatMvpPresenter<V, I> {


    @Inject
    public ChatPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }


    @Override
    public void onSendMessageClick(ItemChat item) {
        if (!isViewAttached()) return;
        if (!TextUtils.isEmpty(item.getMensaje())) getMvpInteractor().addNewMessage(item);
        else getMvpView().showError();
        //getMvpView().showData(getMvpInteractor().getDataFromServer());
    }

    @Override
    public void onViewPrepared() {
        //getMvpView().showData(getMvpInteractor().getDataFromServer());
    }

    @Override
    public void onItemAdded(ItemChat item) {
        if (!isViewAttached()) return;
        getMvpView().addItem(item);
    }
}
