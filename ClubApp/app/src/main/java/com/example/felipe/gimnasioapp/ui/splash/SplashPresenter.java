package com.example.felipe.gimnasioapp.ui.splash;

import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class SplashPresenter<V extends SplashMvpView, I extends SplashMvpInteractor>
        extends BasePresenter<V, I> implements SplashMvpPresenter<V, I> {

    @Inject
    public SplashPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);

        decideNextActivity(getMvpInteractor().getCurrentUserLoggedInMode());

    }

    public void decideNextActivity(boolean isLogged) {
        if (!isViewAttached()) return;
        if (isLogged) getMvpView().openMainActivity();
        else getMvpView().openLoginActivity();
    }

}
