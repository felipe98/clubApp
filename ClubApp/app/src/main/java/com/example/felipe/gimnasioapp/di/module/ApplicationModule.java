package com.example.felipe.gimnasioapp.di.module;

import android.app.Application;
import android.content.Context;

import com.example.felipe.gimnasioapp.BuildConfig;
import com.example.felipe.gimnasioapp.di.ApiInfo;
import com.example.felipe.gimnasioapp.di.ApplicationContext;
import com.example.felipe.gimnasioapp.di.DatabaseInfo;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Felipe on 14/07/2018.
 */
@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }
}
