package com.example.felipe.gimnasioapp.ui.splash;

import android.os.Bundle;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.ui.base.BaseActivity;
import com.example.felipe.gimnasioapp.ui.login.LoginActivity;
import com.example.felipe.gimnasioapp.ui.login.LoginMvpInteractor;
import com.example.felipe.gimnasioapp.ui.login.LoginMvpPresenter;
import com.example.felipe.gimnasioapp.ui.login.LoginMvpView;
import com.example.felipe.gimnasioapp.ui.main.MainActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashMvpView {

    @Inject
    SplashMvpPresenter<SplashMvpView, SplashMvpInteractor> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setUnBinder(ButterKnife.bind(this));

        getActivityComponent().inject(this);

        mPresenter.onAttach(this);

        setUp();

    }

    @Override
    protected void setUp() {

    }

    @Override
    public void openMainActivity() {
        startActivity(MainActivity.getStartIntent(this));
        finish();
    }

    @Override
    public void openLoginActivity() {
        startActivity(LoginActivity.getStartIntent(this));
        finish();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }
}
