package com.example.felipe.gimnasioapp.ui.main;

import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;
import com.example.felipe.gimnasioapp.ui.main.about.AboutFragment;
import com.example.felipe.gimnasioapp.ui.main.addMenu.AddMenuFragment;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestFragment;
import com.example.felipe.gimnasioapp.ui.main.address.AddressFragment;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactFragment;
import com.example.felipe.gimnasioapp.ui.main.detail.DetailFragment;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteFragment;
import com.example.felipe.gimnasioapp.ui.main.order.OrderFragment;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantFragment;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Felipe on 12/07/2018.
 */

public class MainPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends BasePresenter<V, I> implements MainMvpPresenter<V, I> {

    private Realm realm;
    private RealmResults<Restaurant> results;

    @Inject
    public MainPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onDrawerOptionAboutClick() {
        getMvpView().showFragment(new AboutFragment(), "Acerca de");
    }

    @Override
    public void onDrawerChatClick() {
        getMvpView().showFragment(new ContactFragment(), "Contactos");
    }

    @Override
    public void onDrawerOptionLogoutClick() {
        getMvpView().showLoading();
        getMvpInteractor().signOff();
        getMvpInteractor().clearTypeUser();
        getMvpView().openLoginActivity();
    }

    @Override
    public void onDrawerFavoritClick() {
        getMvpView().showFragment(new FavoriteFragment(), "Favoritos");
    }

    @Override
    public void onDrawerOrderClick() {
        getMvpView().showFragment(new OrderFragment(), "Pedidos");
    }

    @Override
    public void onDrawerAddressClick() {
        getMvpView().showFragment(new AddressFragment(), "Dirección");
    }

    @Override
    public void onDrawerEditClick() {
        getMvpView().showFragment(new AddRestFragment(), "Editar restaurante");
    }

    @Override
    public void onDrawerAddMenuClick() {
        getMvpView().showFragment(new AddMenuFragment(),"Agregar menú");
    }

    @Override
    public void onDrawerRateUsClick() {

    }

    @Override
    public void onDrawerRecentClick() {
        getMvpView().showFragment(new RestaurantFragment(), "App restaurante");
    }

    @Override
    public void onRestaurantClick() {
        getMvpView().showFragment(new DetailFragment(),"Mi restaurante");
    }

    @Override
    public void onViewInitialized() {

        if (getMvpInteractor().getTypeUser().equals("RESTAURANT")) {
            getMvpView().hideDrawerItems();
            getMvpView().showFragment(new AddRestFragment(), "Nuevo restaurante");
        }else {
            getMvpView().showFragment(new RestaurantFragment(), "Restaurantes");
        }
    }

}
