package com.example.felipe.gimnasioapp.ui.register.user;

import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

/**
 * Created by Felipe on 19/07/2018.
 */

public interface UserRMvpPresenter<V extends UserRMvpView, I extends UserRMvpInteractor>
        extends MvpPresenter<V, I> {

    void onRegisterClick(User user,String rePassword);
}
