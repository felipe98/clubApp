package com.example.felipe.gimnasioapp.data.model;

public class ItemChat {

    private String id;
    private String mensaje;
    private String nombre;
    private String time;

    public ItemChat() {
    }

    public ItemChat(String id, String mensaje, String nombre, String time) {
        this.id = id;
        this.mensaje = mensaje;
        this.nombre = nombre;
        this.time = time;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
