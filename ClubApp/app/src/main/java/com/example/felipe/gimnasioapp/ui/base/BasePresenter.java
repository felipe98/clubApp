package com.example.felipe.gimnasioapp.ui.base;

import javax.inject.Inject;

/**
 * Created by Felipe on 13/07/2018.
 */

public class BasePresenter<V extends MvpView, I extends MvpInteractor> implements MvpPresenter<V, I> {

    private V mMvpView;
    private I mMvpInteractor;

    @Inject
    public BasePresenter(I mMvpInteractor) {
        this.mMvpInteractor = mMvpInteractor;
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
        mMvpInteractor = null;
    }

    @Override
    public V getMvpView() {
        return mMvpView;
    }

    @Override
    public I getMvpInteractor() {
        return mMvpInteractor;
    }


    @Override
    public boolean isViewAttached() {
        return mMvpView != null;
    }

    @Override
    public void setUserAsLoggedOut() {
        //getMvpInteractor().setAccessToken(null);
    }
}
