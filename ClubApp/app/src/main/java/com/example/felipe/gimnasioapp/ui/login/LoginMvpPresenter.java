package com.example.felipe.gimnasioapp.ui.login;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

/**
 * Created by Felipe on 14/07/2018.
 */

public interface LoginMvpPresenter<V extends LoginMvpView, I extends LoginMvpInteractor>
        extends MvpPresenter<V, I> {

    void onLoginClick(String username, String pass);

    void onRegisterClick();

    void onRememberClick();

}
