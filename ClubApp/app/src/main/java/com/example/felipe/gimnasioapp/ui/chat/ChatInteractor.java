package com.example.felipe.gimnasioapp.ui.chat;

import com.example.felipe.gimnasioapp.data.model.ItemChat;
import com.example.felipe.gimnasioapp.ui.base.BaseInteractor;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ChatInteractor extends BaseInteractor implements ChatMvpInteractor {


    private FirebaseDatabase database;
    private DatabaseReference reference;
    private List<ItemChat> list = new ArrayList<>();

    @Inject
    public ChatInteractor() {
    }

    @Override
    public void searchItem() {

    }

    @Override
    public List<ItemChat> getDataFromServer() {


        return list;
    }

    @Override
    public void addNewMessage(ItemChat itemChat) {
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("messages");

        reference.push().setValue(itemChat);
    }
}
