package com.example.felipe.gimnasioapp.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.ui.base.BaseActivity;
import com.example.felipe.gimnasioapp.ui.login.LoginActivity;
import com.example.felipe.gimnasioapp.ui.main.about.AboutFragment;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteFragment;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantFragment;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements MainMvpView {

    @Inject
    MainMvpPresenter<MainMvpView, MainMvpInteractor> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.drawer_view)
    DrawerLayout mDrawer;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    @BindView(R.id.tv_app_version)
    TextView mAppVersionTextView;

    private TextView mNameTextView;

    private TextView mEmailTextView;

    private CircleImageView mProfileImageView;

    private ActionBarDrawerToggle mDrawerToggle;

    private final int MY_PERMISSIONS_REQUEST_LOCATION = 0;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();

    }

    private void setPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, "rechazó permiso", Toast.LENGTH_SHORT).show();

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "permiso aceptado", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(this, "permiso denegado", Toast.LENGTH_SHORT).show();
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void toolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("App restaurant");
        }
    }

    @Override
    public void openLoginActivity() {
        startActivity(LoginActivity.getStartIntent(this));
        finish();
    }

    @Override
    public void showFragment(Fragment fragment, String title) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();

        getSupportActionBar().setTitle(title);
    }

    @Override
    public void hideDrawerItems() {

        Menu nav_Menu = mNavigationView.getMenu();

        nav_Menu.findItem(R.id.favorite).setVisible(false);
        nav_Menu.findItem(R.id.address).setVisible(false);
        nav_Menu.findItem(R.id.restaurants).setVisible(false);

        nav_Menu.findItem(R.id.edit).setVisible(true);
        nav_Menu.findItem(R.id.restaurant).setVisible(true);
        nav_Menu.findItem(R.id.addMenu).setVisible(true);
    }

    @Override
    public void updateUserName(String currentUserName) {

    }

    @Override
    public void updateUserEmail(String currentUserEmail) {

    }

    @Override
    public void updateUserProfilePic(String currentUserProfilePicUrl) {

    }

    @Override
    public void updateAppVersion() {

    }

    @Override
    public void showRateUsDialog() {

    }

    @Override
    protected void setUp() {
        toolbar();

        setPermissionGranted();

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawer,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        setupNavMenu();

        mPresenter.onViewInitialized();
    }

    void setupNavMenu() {

        View headerLayout = mNavigationView.getHeaderView(0);

        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        mDrawer.closeDrawer(GravityCompat.START);
                        switch (item.getItemId()) {
                            case R.id.restaurants:
                                mPresenter.onDrawerRecentClick();
                                break;
                            case R.id.restaurant:
                                mPresenter.onRestaurantClick();
                                break;
                            case R.id.order:
                                mPresenter.onDrawerOrderClick();
                                break;
                            case R.id.favorite:
                                mPresenter.onDrawerFavoritClick();
                                break;
                            case R.id.address:
                                mPresenter.onDrawerAddressClick();
                                break;
                            case R.id.edit:
                                mPresenter.onDrawerEditClick();
                                break;
                            case R.id.addMenu:
                                mPresenter.onDrawerAddMenuClick();
                                break;
                            case R.id.logout:
                                mPresenter.onDrawerOptionLogoutClick();
                                break;
                            case R.id.about:
                                mPresenter.onDrawerOptionAboutClick();
                                break;
                        }
                        return true;
                    }
                });
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
