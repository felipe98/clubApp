package com.example.felipe.gimnasioapp.ui.register.user;

import android.text.TextUtils;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;
import com.example.felipe.gimnasioapp.utils.CommonUtils;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by Felipe on 19/07/2018.
 */

public class UserRPresenter<V extends UserRMvpView, I extends UserRMvpInteractor>
        extends BasePresenter<V, I> implements UserRMvpPresenter<V, I> {

    private Realm realm;

    @Inject
    public UserRPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }


    @Override
    public void onRegisterClick(User user, String rePassword) {

        realm = Realm.getDefaultInstance();

        if (TextUtils.isEmpty(user.getName())) {
            getMvpView().showNameEmptyError();
            return;
        }

        if (TextUtils.isEmpty(user.getLastName())) {
            getMvpView().showLastNameEmptyError();
            return;
        }

        if (TextUtils.isEmpty(user.getEmail())) {
            getMvpView().showEmailEmptyError();
            return;
        }

        if (TextUtils.isEmpty(user.getPass())) {
            getMvpView().showPasswordEmptyError();
            return;
        }

        if (TextUtils.isEmpty(rePassword)) {
            getMvpView().showRePasswordEmptyError();
            return;
        }


        if (!user.getPass().equals(rePassword)) {
            getMvpView().showDifferentRePasswordError();
            return;
        }

        if (!CommonUtils.isEmailValid(user.getEmail())) {
            getMvpView().showFormatEmailError();
            return;
        }

        getMvpView().showLoading();

        getMvpInteractor().registerUser(user, new UserRMvpInteractor.FirebaseListener() {
            @Override
            public void newUser(boolean user) {

                if (!isViewAttached()) return;

                if (user) {
                    getMvpView().hideLoading();
                    addTypeUser(new TypeUser("USER"));
                    getMvpView().openMainActivity();
                }
            }
        });


    }

    private void addTypeUser(TypeUser item) {
        realm.beginTransaction();
        realm.copyToRealm(item);
        realm.commitTransaction();
    }
}
