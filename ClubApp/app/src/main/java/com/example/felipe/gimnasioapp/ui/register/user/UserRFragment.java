package com.example.felipe.gimnasioapp.ui.register.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.di.component.ActivityComponent;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;
import com.example.felipe.gimnasioapp.ui.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserRFragment extends BaseFragment implements UserRMvpView {

    @Inject
    UserRPresenter<UserRMvpView, UserRMvpInteractor> mPresenter;

    @BindView(R.id.iLName)
    TextInputLayout iLName;
    @BindView(R.id.inputName)
    EditText name;

    @BindView(R.id.iLLastName)
    TextInputLayout iLLastName;
    @BindView(R.id.inputAp)
    EditText lastName;

    @BindView(R.id.iLEmail)
    TextInputLayout iLEmail;
    @BindView(R.id.inputEmail)
    EditText email;

    @BindView(R.id.iLpass)
    TextInputLayout iLPass;
    @BindView(R.id.inputPassword)
    EditText pass;

    @BindView(R.id.iLRePass)
    TextInputLayout iLRePass;
    @BindView(R.id.inputRePassword)
    EditText rePass;

    public UserRFragment() {
    }

    public static UserRFragment newInstance() {
        Bundle args = new Bundle();
        UserRFragment fragment = new UserRFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_r, container, false);

        ActivityComponent component = getActivityComponent();

        if (component != null) {

            component.inject(this);

            setUnBinder(ButterKnife.bind(this, rootView));

            mPresenter.onAttach(this);
        }

        return rootView;
    }

    @OnClick(R.id.bt_go)
    public void onRegisterClick() {

        mPresenter.onRegisterClick(new User(name.getText().toString(), lastName.getText().toString(),"",
                        "","", email.getText().toString(), pass.getText().toString(),"","USER"
                        ),
                rePass.getText().toString());

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void openMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra("item", new TypeUser("user"));
        startActivity(intent);
    }

    @Override
    public void showDifferentRePasswordError() {
        iLRePass.setError("Contraseña no coincide");
    }

    @Override
    public void showFormatEmailError() {
        iLEmail.setError("Formato incorrecto");
    }

    @Override
    public void showNameEmptyError() {
        iLName.setError("Nombre inválido");
    }

    @Override
    public void showLastNameEmptyError() {
        iLLastName.setError("Apellido inválido");
    }

    @Override
    public void showEmailEmptyError() {
        iLEmail.setError("Email inválido");
    }

    @Override
    public void showPasswordEmptyError() {
        iLPass.setError("Contraseña inválido");
    }

    @Override
    public void showRePasswordEmptyError() {
        iLRePass.setError("Re-contraseña inválido");
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
