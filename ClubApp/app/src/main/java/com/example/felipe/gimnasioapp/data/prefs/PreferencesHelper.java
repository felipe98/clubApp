package com.example.felipe.gimnasioapp.data.prefs;

public interface PreferencesHelper {

    int getCurrentUserLoggedInMode();

    //void setCurrentUserLoggedInMode(AppConstants.LoggedInMode mode);

    Long getCurrentUserId();

    void setCurrentUserId(Long userId);

    String getCurrentUserName();

    void setCurrentUserName(String userName);

    String getCurrentUserEmail();

    void setCurrentUserEmail(String email);

    String getCurrentUserProfilePicUrl();

    void setCurrentUserProfilePicUrl(String profilePicUrl);

    String getAccessToken();

    void setAccessToken(String accessToken);
}
