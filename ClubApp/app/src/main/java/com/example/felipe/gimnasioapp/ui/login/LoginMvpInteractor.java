package com.example.felipe.gimnasioapp.ui.login;

import android.support.annotation.NonNull;

import com.example.felipe.gimnasioapp.ui.base.MvpInteractor;

/**
 * Created by Felipe on 14/07/2018.
 */

public interface  LoginMvpInteractor extends MvpInteractor {

    interface LoginFirebaseListener {

        void loginUser(boolean user);

    }

    void loginUser(String email, String pass, @NonNull LoginFirebaseListener listener);
}
