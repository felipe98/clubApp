package com.example.felipe.gimnasioapp.data.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    private String name;
    private String lastName;
    private String category;
    private String clubCode;
    private String postalCode;
    private String email;
    private String pass;
    private String avatar;
    private String type;

    public User() {
    }

    public User(String name, String lastName, String category, String clubCode, String postalCode,
                String email, String pass, String avatar, String type) {
        this.name = name;
        this.lastName = lastName;
        this.category = category;
        this.clubCode = clubCode;
        this.postalCode = postalCode;
        this.email = email;
        this.pass = pass;
        this.avatar = avatar;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getClubCode() {
        return clubCode;
    }

    public void setClubCode(String clubCode) {
        this.clubCode = clubCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}