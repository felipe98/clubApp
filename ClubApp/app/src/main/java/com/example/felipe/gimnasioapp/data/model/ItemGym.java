package com.example.felipe.gimnasioapp.data.model;

/**
 * Created by Felipe on 16/07/2018.
 */

public class ItemGym {

    private int id;
    private String title;
    private String image;
    private boolean state;

    public ItemGym(int id, String title, String image, boolean state) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
