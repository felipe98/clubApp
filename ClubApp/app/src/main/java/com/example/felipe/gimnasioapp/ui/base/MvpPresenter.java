package com.example.felipe.gimnasioapp.ui.base;

/**
 * Created by Felipe on 12/07/2018.
 */

public interface MvpPresenter <V extends MvpView, I extends MvpInteractor> {

    void onAttach(V mvpView);

    void onDetach();

    V getMvpView();

    I getMvpInteractor();

    boolean isViewAttached();

    void setUserAsLoggedOut();
}
