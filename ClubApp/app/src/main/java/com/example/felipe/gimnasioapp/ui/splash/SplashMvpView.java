package com.example.felipe.gimnasioapp.ui.splash;

import com.example.felipe.gimnasioapp.ui.base.MvpView;

public interface SplashMvpView extends MvpView {

    void openMainActivity();

    void openLoginActivity();
}
