package com.example.felipe.gimnasioapp.ui.splash;

import com.example.felipe.gimnasioapp.ui.base.MvpInteractor;

public interface SplashMvpInteractor extends MvpInteractor{

    boolean getCurrentUserLoggedInMode();

}
