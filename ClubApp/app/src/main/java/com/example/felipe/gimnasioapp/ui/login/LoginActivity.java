package com.example.felipe.gimnasioapp.ui.login;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.StaticConfig;
import com.example.felipe.gimnasioapp.ui.base.BaseActivity;
import com.example.felipe.gimnasioapp.ui.chat.ChatMvpInteractor;
import com.example.felipe.gimnasioapp.ui.chat.ChatMvpPresenter;
import com.example.felipe.gimnasioapp.ui.chat.ChatMvpView;
import com.example.felipe.gimnasioapp.ui.main.MainActivity;
import com.example.felipe.gimnasioapp.ui.main.MainMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.MainMvpView;
import com.example.felipe.gimnasioapp.ui.main.MainPresenter;
import com.example.felipe.gimnasioapp.ui.register.RegisterActivity;
import com.example.felipe.gimnasioapp.ui.remember.RememberActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginMvpView {

    @Inject
    LoginMvpPresenter<LoginMvpView, LoginMvpInteractor> mPresenter;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.et_username)
    EditText username;

    @BindView(R.id.et_password)
    EditText password;

    private String name;
    private String pass;


    public static Intent getStartIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setUnBinder(ButterKnife.bind(this));

        getActivityComponent().inject(this);

        mPresenter.onAttach(this);

        setUp();

    }

    @OnClick(R.id.remember)
    public void onRememberClick() {
        mPresenter.onRememberClick();
    }

    @OnClick(R.id.bt_go)
    public void onLoginClick() {

        name = username.getText().toString();
        pass = password.getText().toString();

        mPresenter.onLoginClick(name, pass);

    }

    @OnClick(R.id.fab)
    public void onAddClick() {
        getWindow().setExitTransition(null);
        getWindow().setEnterTransition(null);
        mPresenter.onRegisterClick();
    }

    @Override
    protected void setUp() {

    }

    @Override
    public void openMainActivity() {
        startActivity(MainActivity.getStartIntent(this));
        finish();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void openRegisterActivity() {
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, fab, fab.getTransitionName());
        startActivity(RegisterActivity.getStartIntent(this), options.toBundle());
    }

    @Override
    public void openRememberActivity() {
        startActivity(RememberActivity.getStartIntent(this));
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }
}
