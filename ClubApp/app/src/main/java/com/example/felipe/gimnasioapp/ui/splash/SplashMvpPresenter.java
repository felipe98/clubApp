package com.example.felipe.gimnasioapp.ui.splash;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface SplashMvpPresenter <V extends SplashMvpView, I extends SplashMvpInteractor>
        extends MvpPresenter<V, I> {


}
