package com.example.felipe.gimnasioapp.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by Felipe on 12/07/2018.
 */

public interface MvpView {

    void showLoading();

    void hideLoading();

    void openActivityOnTokenExpire();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();

}
