package com.example.felipe.gimnasioapp.ui.main;

import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.utils.CommonUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Felipe on 12/07/2018.
 */

public class MainInteractor implements MainMvpInteractor {

    private FirebaseAuth mAuth;
    private Realm realm;

    @Inject
    public MainInteractor() {
    }

    @Override
    public void searchItem() {
    }

    @Override
    public String getCurrentUserName() {
        return null;
    }

    @Override
    public String getCurrentUserEmail() {
        return null;
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return null;
    }

    @Override
    public String getTypeUser() {
        realm = Realm.getDefaultInstance();
        TypeUser rows = realm.where(TypeUser.class).findFirst();
        return rows.getType();
    }

    @Override
    public void clearTypeUser() {
        realm.beginTransaction();
        RealmResults<TypeUser> rows = realm.where(TypeUser.class).findAll();
        rows.deleteAllFromRealm();
        realm.commitTransaction();
    }

    @Override
    public void signOff() {
        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
    }
}
