package com.example.felipe.gimnasioapp.ui.main;

import android.support.v4.app.Fragment;

import com.example.felipe.gimnasioapp.ui.base.MvpView;

/**
 * Created by Felipe on 12/07/2018.
 */

public interface MainMvpView extends MvpView {

    void openLoginActivity();

    void showFragment(Fragment fragment, String  title);

    void hideDrawerItems();

    void updateUserName(String currentUserName);

    void updateUserEmail(String currentUserEmail);

    void updateUserProfilePic(String currentUserProfilePicUrl);

    void updateAppVersion();

    void showRateUsDialog();
}
