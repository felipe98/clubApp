package com.example.felipe.gimnasioapp.ui.register;

import com.example.felipe.gimnasioapp.ui.base.BasePresenter;
import com.example.felipe.gimnasioapp.ui.main.MainMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.MainMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.MainMvpView;

import javax.inject.Inject;

/**
 * Created by Felipe on 15/07/2018.
 */

public class RegisterPresenter<V extends RegisterMvpView, I extends RegisterMvpInteractor>
        extends BasePresenter<V, I> implements RegisterMvpPresenter<V, I> {

    private int pos;

    @Inject
    public RegisterPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onPageChanged(int check) {
        getMvpView().changePage(check);
        this.pos = check;
    }

    @Override
    public void onCheckChanged() {
        if (pos == 0) getMvpView().checkUserBox(true);
        else getMvpView().checkGymBox(true);
    }
}
