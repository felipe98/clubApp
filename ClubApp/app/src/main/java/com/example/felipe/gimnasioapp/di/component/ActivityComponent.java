package com.example.felipe.gimnasioapp.di.component;

import com.example.felipe.gimnasioapp.di.PerActivity;
import com.example.felipe.gimnasioapp.di.module.ActivityModule;
import com.example.felipe.gimnasioapp.ui.chat.ChatActivity;
import com.example.felipe.gimnasioapp.ui.detail.DetailActivity;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodFragment;
import com.example.felipe.gimnasioapp.ui.detail.rate.RateFragment;
import com.example.felipe.gimnasioapp.ui.filter.FilterActivity;
import com.example.felipe.gimnasioapp.ui.login.LoginActivity;
import com.example.felipe.gimnasioapp.ui.main.MainActivity;
import com.example.felipe.gimnasioapp.ui.main.about.AboutFragment;
import com.example.felipe.gimnasioapp.ui.main.addMenu.AddMenuFragment;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestFragment;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteFragment;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantFragment;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactFragment;
import com.example.felipe.gimnasioapp.ui.map.MapActivity;
import com.example.felipe.gimnasioapp.ui.register.RegisterActivity;
import com.example.felipe.gimnasioapp.ui.register.restaurant.RestaurantRFragment;
import com.example.felipe.gimnasioapp.ui.register.user.UserRFragment;
import com.example.felipe.gimnasioapp.ui.splash.SplashActivity;

import dagger.Component;

/**
 * Created by Felipe on 14/07/2018.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(LoginActivity activity);

    void inject(RegisterActivity activity);

    void inject(DetailActivity activity);

    void inject(ChatActivity activity);

    void inject(MapActivity activity);

    void inject(FilterActivity activity);

    void inject(ContactFragment fragment);

    void inject(AddRestFragment fragment);

    void inject(AboutFragment fragment);

    void inject(FoodFragment fragment);

    void inject(AddMenuFragment fragment);

    void inject(RateFragment fragment);

    void inject(FavoriteFragment fragment);

    void inject(RestaurantFragment fragment);

    void inject(RestaurantRFragment fragment);

    void inject(UserRFragment fragment);

    void inject(SplashActivity activity);
}

