package com.example.felipe.gimnasioapp.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Felipe on 14/07/2018.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerService {
}

