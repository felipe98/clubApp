package com.example.felipe.gimnasioapp.ui.register;

import com.example.felipe.gimnasioapp.ui.base.MvpView;

/**
 * Created by Felipe on 15/07/2018.
 */

public interface RegisterMvpView extends MvpView {

    void changePage(int check);

    void checkUserBox(boolean check);

    void checkGymBox(boolean check);
}
