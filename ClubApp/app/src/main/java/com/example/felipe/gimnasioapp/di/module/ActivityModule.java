package com.example.felipe.gimnasioapp.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.example.felipe.gimnasioapp.data.model.Commentary;
import com.example.felipe.gimnasioapp.data.model.Food;
import com.example.felipe.gimnasioapp.data.model.ItemChat;
import com.example.felipe.gimnasioapp.data.model.ItemFilter;
import com.example.felipe.gimnasioapp.data.model.ItemGym;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.di.ActivityContext;
import com.example.felipe.gimnasioapp.di.PerActivity;
import com.example.felipe.gimnasioapp.ui.chat.ChatAdapter;
import com.example.felipe.gimnasioapp.ui.chat.ChatInteractor;
import com.example.felipe.gimnasioapp.ui.chat.ChatMvpInteractor;
import com.example.felipe.gimnasioapp.ui.chat.ChatMvpPresenter;
import com.example.felipe.gimnasioapp.ui.chat.ChatMvpView;
import com.example.felipe.gimnasioapp.ui.chat.ChatPresenter;
import com.example.felipe.gimnasioapp.ui.detail.DetailInteractor;
import com.example.felipe.gimnasioapp.ui.detail.DetailMvpInteractor;
import com.example.felipe.gimnasioapp.ui.detail.DetailMvpPresenter;
import com.example.felipe.gimnasioapp.ui.detail.DetailMvpView;
import com.example.felipe.gimnasioapp.ui.detail.DetailPresenter;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodAdapter;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodInteractor;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodMvpInteractor;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodMvpPresenter;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodMvpView;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodPresenter;
import com.example.felipe.gimnasioapp.ui.detail.rate.RateAdapter;
import com.example.felipe.gimnasioapp.ui.detail.rate.RateInteractor;
import com.example.felipe.gimnasioapp.ui.detail.rate.RateMvpInteractor;
import com.example.felipe.gimnasioapp.ui.detail.rate.RateMvpPresenter;
import com.example.felipe.gimnasioapp.ui.detail.rate.RateMvpView;
import com.example.felipe.gimnasioapp.ui.detail.rate.RatePresenter;
import com.example.felipe.gimnasioapp.ui.filter.FilterAdapter;
import com.example.felipe.gimnasioapp.ui.filter.FilterInteractor;
import com.example.felipe.gimnasioapp.ui.filter.FilterMvpInteractor;
import com.example.felipe.gimnasioapp.ui.filter.FilterMvpPresenter;
import com.example.felipe.gimnasioapp.ui.filter.FilterMvpView;
import com.example.felipe.gimnasioapp.ui.filter.FilterPresenter;
import com.example.felipe.gimnasioapp.ui.login.LoginInteractor;
import com.example.felipe.gimnasioapp.ui.login.LoginMvpInteractor;
import com.example.felipe.gimnasioapp.ui.login.LoginMvpPresenter;
import com.example.felipe.gimnasioapp.ui.login.LoginMvpView;
import com.example.felipe.gimnasioapp.ui.login.LoginPresenter;
import com.example.felipe.gimnasioapp.ui.main.MainInteractor;
import com.example.felipe.gimnasioapp.ui.main.MainMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.MainMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.MainMvpView;
import com.example.felipe.gimnasioapp.ui.main.MainPresenter;
import com.example.felipe.gimnasioapp.ui.main.about.AboutInteractor;
import com.example.felipe.gimnasioapp.ui.main.about.AboutMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.about.AboutMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.about.AboutMvpView;
import com.example.felipe.gimnasioapp.ui.main.about.AboutPresenter;
import com.example.felipe.gimnasioapp.ui.main.addMenu.AddMenuInteractor;
import com.example.felipe.gimnasioapp.ui.main.addMenu.AddMenuMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.addMenu.AddMenuMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.addMenu.AddMenuMvpView;
import com.example.felipe.gimnasioapp.ui.main.addMenu.AddMenuPresenter;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestInteractor;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestMvpView;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestPresenter;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteAdapter;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteInteractor;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteMvpView;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoritePresenter;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantAdapter;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantInteractor;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantMvpView;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantPresenter;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactAdapter;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactInteractor;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactMvpView;
import com.example.felipe.gimnasioapp.ui.main.contact.ContactPresenter;
import com.example.felipe.gimnasioapp.ui.map.MapInteractor;
import com.example.felipe.gimnasioapp.ui.map.MapMvpInteractor;
import com.example.felipe.gimnasioapp.ui.map.MapMvpPresenter;
import com.example.felipe.gimnasioapp.ui.map.MapMvpView;
import com.example.felipe.gimnasioapp.ui.map.MapPresenter;
import com.example.felipe.gimnasioapp.ui.register.RegisterInteractor;
import com.example.felipe.gimnasioapp.ui.register.RegisterMvpInteractor;
import com.example.felipe.gimnasioapp.ui.register.RegisterMvpPresenter;
import com.example.felipe.gimnasioapp.ui.register.RegisterMvpView;
import com.example.felipe.gimnasioapp.ui.register.RegisterPresenter;
import com.example.felipe.gimnasioapp.ui.register.restaurant.RestaurantRInteractor;
import com.example.felipe.gimnasioapp.ui.register.restaurant.RestaurantRMvpInteractor;
import com.example.felipe.gimnasioapp.ui.register.restaurant.RestaurantRMvpPresenter;
import com.example.felipe.gimnasioapp.ui.register.restaurant.RestaurantRMvpView;
import com.example.felipe.gimnasioapp.ui.register.restaurant.RestaurantRPresenter;
import com.example.felipe.gimnasioapp.ui.register.user.UserRInteractor;
import com.example.felipe.gimnasioapp.ui.register.user.UserRMvpInteractor;
import com.example.felipe.gimnasioapp.ui.register.user.UserRMvpPresenter;
import com.example.felipe.gimnasioapp.ui.register.user.UserRMvpView;
import com.example.felipe.gimnasioapp.ui.register.user.UserRPresenter;
import com.example.felipe.gimnasioapp.ui.splash.SplashInteractor;
import com.example.felipe.gimnasioapp.ui.splash.SplashMvpInteractor;
import com.example.felipe.gimnasioapp.ui.splash.SplashMvpPresenter;
import com.example.felipe.gimnasioapp.ui.splash.SplashMvpView;
import com.example.felipe.gimnasioapp.ui.splash.SplashPresenter;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Felipe on 14/07/2018.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    //Splash
    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView, SplashMvpInteractor> provideSplashPresenter(
            SplashPresenter<SplashMvpView, SplashMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SplashMvpInteractor provideSplashMvpInteractor(SplashInteractor interactor) {
        return interactor;
    }

    //Main
    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView, MainMvpInteractor> provideMainPresenter(MainPresenter<MainMvpView,
            MainMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpInteractor provideMainMvpInteractor(MainInteractor interactor) {
        return interactor;
    }

    //Login
    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView, LoginMvpInteractor> provideLoginPresenter(LoginPresenter<LoginMvpView,
            LoginMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpInteractor provideLoginMvpInteractor(LoginInteractor interactor) {
        return interactor;
    }

    //Register
    @Provides
    RegisterMvpPresenter<RegisterMvpView, RegisterMvpInteractor> provideRegisterPresenter(RegisterPresenter<RegisterMvpView,
            RegisterMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RegisterMvpInteractor provideRegisterMvpInteractor(RegisterInteractor interactor) {
        return interactor;
    }

    //Detail
    @Provides
    @PerActivity
    DetailMvpInteractor provideDetailMvpInteractor(DetailInteractor interactor) {
        return interactor;
    }

    //Register
    @Provides
    DetailMvpPresenter<DetailMvpView, DetailMvpInteractor> provideDetailPresenter(DetailPresenter<DetailMvpView,
            DetailMvpInteractor> presenter) {
        return presenter;
    }

    //Club
    @Provides
    @PerActivity
    RestaurantMvpPresenter<RestaurantMvpView, RestaurantMvpInteractor> provideClubPresenter(RestaurantPresenter<RestaurantMvpView,
            RestaurantMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RestaurantMvpInteractor provideClubMvpInteractor(RestaurantInteractor interactor) {
        return interactor;
    }

    //Favorite
    @Provides
    @PerActivity
    FavoriteMvpPresenter<FavoriteMvpView, FavoriteMvpInteractor> provideFavoritePresenter(FavoritePresenter<FavoriteMvpView,
            FavoriteMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    FavoriteMvpInteractor provideFavoriteMvpInteractor(FavoriteInteractor interactor) {
        return interactor;
    }

    //Map
    @Provides
    @PerActivity
    MapMvpPresenter<MapMvpView, MapMvpInteractor> provideMapPresenter(MapPresenter<MapMvpView,
                MapMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MapMvpInteractor provideMapMvpInteractor(MapInteractor interactor) {
        return interactor;
    }

    //Add menu
    @Provides
    @PerActivity
    AddMenuMvpPresenter<AddMenuMvpView, AddMenuMvpInteractor> provideAddMenuPresenter(AddMenuPresenter<AddMenuMvpView,
            AddMenuMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AddMenuMvpInteractor provideAddMenuMvpInteractor(AddMenuInteractor interactor) {
        return interactor;
    }

    //Contact
    @Provides
    @PerActivity
    ContactMvpPresenter<ContactMvpView, ContactMvpInteractor> provideContactPresenter(ContactPresenter<ContactMvpView,
            ContactMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ContactMvpInteractor provideContactMvpInteractor(ContactInteractor interactor) {
        return interactor;
    }

    //Chat
    @Provides
    @PerActivity
    ChatMvpPresenter<ChatMvpView, ChatMvpInteractor> provideChatPresenter(ChatPresenter<ChatMvpView,
            ChatMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ChatMvpInteractor provideChatMvpInteractor(ChatInteractor interactor) {
        return interactor;
    }

    //ItemFilter
    @Provides
    @PerActivity
    FilterMvpPresenter<FilterMvpView, FilterMvpInteractor> provideFilterPresenter(FilterPresenter<FilterMvpView,
            FilterMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    FilterMvpInteractor provideFilterMvpInteractor(FilterInteractor interactor) {
        return interactor;
    }

    //Restaurant register
    @Provides
    @PerActivity
    RestaurantRMvpPresenter<RestaurantRMvpView, RestaurantRMvpInteractor> provideClubRPresenter(RestaurantRPresenter<RestaurantRMvpView,
            RestaurantRMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RestaurantRMvpInteractor provideClubRMvpInteractor(RestaurantRInteractor interactor) {
        return interactor;
    }

    //User register
    @Provides
    @PerActivity
    UserRMvpPresenter<UserRMvpView, UserRMvpInteractor> provideUserRPresenter(UserRPresenter<UserRMvpView,
            UserRMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    UserRMvpInteractor provideUserRMvpInteractor(UserRInteractor interactor) {
        return interactor;
    }

    //Menu restaurant
    @Provides
    @PerActivity
    FoodMvpPresenter<FoodMvpView, FoodMvpInteractor> provideMenuPresenter(FoodPresenter<FoodMvpView,
            FoodMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    FoodMvpInteractor provideMenuMvpInteractor(FoodInteractor interactor) {
        return interactor;
    }

    //Add restaurant
    @Provides
    @PerActivity
    AddRestMvpPresenter<AddRestMvpView, AddRestMvpInteractor> provideAddRestPresenter(AddRestPresenter<AddRestMvpView,
                AddRestMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AddRestMvpInteractor provideAddRestMvpInteractor(AddRestInteractor interactor) {
        return interactor;
    }

    //About
    @Provides
    @PerActivity
    AboutMvpPresenter<AboutMvpView, AboutMvpInteractor> provideAboutPresenter(AboutPresenter<AboutMvpView,
            AboutMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AboutMvpInteractor provideAboutMvpInteractor(AboutInteractor interactor) {
        return interactor;
    }

    //Rate restaurant
    @Provides
    @PerActivity
    RateMvpPresenter<RateMvpView, RateMvpInteractor> provideRatePresenter(RatePresenter<RateMvpView,
            RateMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RateMvpInteractor provideRateMvpInteractor(RateInteractor interactor) {
        return interactor;
    }

    //Restaurant adapter
    @Provides
    RestaurantAdapter provideRestaurantAdapter() {
        return new RestaurantAdapter(new ArrayList<Restaurant>());
    }

    //Favorite adapter
    @Provides
    FavoriteAdapter provideFavoritetAdapter() {
        return new FavoriteAdapter(new ArrayList<Restaurant>());
    }

    //Chat adapter
    @Provides
    ChatAdapter provideChatAdapter() {
        return new ChatAdapter(new ArrayList<ItemChat>());
    }

    //Contact adapter
    @Provides
    ContactAdapter provideContactAdapter() {
        return new ContactAdapter(new ArrayList<User>());
    }

    //Food/restaurant adapter
    @Provides
    FoodAdapter provideMenuAdapter() {
        return new FoodAdapter(new ArrayList<Food>());
    }

    //Rate adapter
    @Provides
    RateAdapter provideRateAdapter() {
        return new RateAdapter(new ArrayList<Commentary>());
    }

    //Filter Adapter
    @Provides
    FilterAdapter provideFilterAdapter() {
        return new FilterAdapter(new ArrayList<ItemFilter>());
    }

    //Layout manager
    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

}
