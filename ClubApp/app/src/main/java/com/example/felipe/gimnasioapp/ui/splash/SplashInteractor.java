package com.example.felipe.gimnasioapp.ui.splash;

import com.example.felipe.gimnasioapp.ui.base.BaseInteractor;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

public class SplashInteractor extends BaseInteractor implements SplashMvpInteractor {

    private FirebaseAuth mAuth;

    @Inject
    public SplashInteractor() {
    }

    @Override
    public boolean getCurrentUserLoggedInMode() {
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        return user != null;
    }

    @Override
    public void searchItem() {

    }
}
