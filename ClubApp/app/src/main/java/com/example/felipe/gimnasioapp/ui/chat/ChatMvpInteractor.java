package com.example.felipe.gimnasioapp.ui.chat;

import com.example.felipe.gimnasioapp.data.model.ItemChat;
import com.example.felipe.gimnasioapp.ui.base.MvpInteractor;

import java.util.List;

public interface ChatMvpInteractor extends MvpInteractor {

    List<ItemChat> getDataFromServer();

    void addNewMessage(ItemChat item);
}
