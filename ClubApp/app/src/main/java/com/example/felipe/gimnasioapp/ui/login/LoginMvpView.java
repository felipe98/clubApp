package com.example.felipe.gimnasioapp.ui.login;

import com.example.felipe.gimnasioapp.ui.base.MvpView;

/**
 * Created by Felipe on 14/07/2018.
 */

public interface LoginMvpView extends MvpView {

    void openMainActivity();

    void openRegisterActivity();

    void openRememberActivity();

}
