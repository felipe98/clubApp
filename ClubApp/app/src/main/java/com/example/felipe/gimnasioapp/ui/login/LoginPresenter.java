package com.example.felipe.gimnasioapp.ui.login;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;
import com.example.felipe.gimnasioapp.utils.CommonUtils;

import javax.inject.Inject;

/**
 * Created by Felipe on 14/07/2018.
 */

public class LoginPresenter<V extends LoginMvpView, I extends LoginMvpInteractor>
        extends BasePresenter<V, I> implements LoginMvpPresenter<V, I> {

    @Inject
    public LoginPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }


    @Override
    public void onLoginClick(String username, String pass) {

        if (username == null || username.isEmpty()) {
            getMvpView().onError(R.string.empty_email);
            return;
        }
        if (!CommonUtils.isEmailValid(username)) {
            getMvpView().onError(R.string.invalid_email);
            return;
        }
        if (pass == null || pass.isEmpty()) {
            getMvpView().onError(R.string.empty_password);
            return;
        }

        getMvpView().showLoading();

        getMvpInteractor().loginUser(username, pass, new LoginMvpInteractor.LoginFirebaseListener() {
            @Override
            public void loginUser(boolean user) {

                if (!isViewAttached()) return;

                if (!user) {
                    getMvpView().onError(R.string.unregistered_user);
                } else {
                    getMvpView().openMainActivity();
                }
                getMvpView().hideLoading();
            }
        });

    }

    @Override
    public void onRegisterClick() {
        getMvpView().openRegisterActivity();
    }

    @Override
    public void onRememberClick() {
        getMvpView().openRememberActivity();
    }
}
