package com.example.felipe.gimnasioapp.ui.chat;

import com.example.felipe.gimnasioapp.data.model.ItemChat;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import java.util.List;

public interface ChatMvpView extends MvpView{

    void showError();

    void setText();

    void showData(List<ItemChat> data);

    void addItem(ItemChat item);

}
