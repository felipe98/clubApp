package com.example.felipe.gimnasioapp.ui.main.contact;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.di.component.ActivityComponent;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactFragment extends BaseFragment implements  ContactMvpView {

    @Inject
    ContactMvpPresenter<ContactMvpView, ContactMvpInteractor> mPresenter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    ContactAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;

    public ContactFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact, container, false);

        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, rootView));
            mPresenter.onAttach(this);

        }


        return rootView;
    }

    @Override
    protected void setUp(View view) {

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.onViewPrepared();

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateContact(List<User> list) {
        adapter.addItems(list);
    }
}
