package com.example.felipe.gimnasioapp.ui.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.ItemChat;
import com.example.felipe.gimnasioapp.ui.base.BaseActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatActivity extends BaseActivity implements ChatMvpView {

    @Inject
    ChatMvpPresenter<ChatMvpView, ChatMvpInteractor> mPresenter;

    @Inject
    ChatAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.editText)
    EditText editText;

    private String message;
    private FirebaseDatabase database;
    private DatabaseReference reference;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, ChatActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        setUnBinder(ButterKnife.bind(this));

        getActivityComponent().inject(this);

        mPresenter.onAttach(this);

        setUp();

        firebaseListener();

    }

    private void firebaseListener() {
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ItemChat item = dataSnapshot.getValue(ItemChat.class);
                mPresenter.onItemAdded(item);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void toolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
    }

    @OnClick(R.id.send)
    public void onSendClick() {
        message = editText.getText().toString();

        mPresenter.onSendMessageClick(new ItemChat("", message, "", ""));

        editText.setText("");
    }

    @Override
    protected void setUp() {
        toolbar();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("messages");

        mPresenter.onViewPrepared();

    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showError() {
        Toast.makeText(this, "Mensaje vacío", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setText() {
        editText.setText("");
    }

    @Override
    public void showData(final List<ItemChat> data) {

    }

    @Override
    public void addItem(ItemChat item) {
        adapter.addItem(item);
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
    }
}
