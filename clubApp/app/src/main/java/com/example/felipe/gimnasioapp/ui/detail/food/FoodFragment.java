package com.example.felipe.gimnasioapp.ui.detail.food;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.Food;
import com.example.felipe.gimnasioapp.di.component.ActivityComponent;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FoodFragment extends BaseFragment implements FoodMvpView {

    @Inject
    FoodMvpPresenter<FoodMvpView, FoodMvpInteractor> mPresenter;

    @Inject
    FoodAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.editSearch)
    EditText txtSearch;

    @BindView(R.id.clearText)
    ImageView clear;

    public FoodFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_food, container, false);

        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, rootView));
            mPresenter.onAttach(this);
            //mBlogAdapter.setCallback(this);
        }

        return rootView;
    }

    public void initSearchView() {
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) clear.setVisibility(View.VISIBLE);
                else clear.setVisibility(View.GONE);

                adapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void setUp(View view) {
        initSearchView();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.onViewPrepared();

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateFood(List<Food> list) {
        adapter.addItems(list);
    }
}
