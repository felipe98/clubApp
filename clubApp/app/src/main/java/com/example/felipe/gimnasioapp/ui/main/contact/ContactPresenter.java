package com.example.felipe.gimnasioapp.ui.main.contact;

import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ContactPresenter<V extends ContactMvpView, I extends ContactMvpInteractor>
        extends BasePresenter<V, I> implements ContactMvpPresenter<V, I> {

    List<User> list = new ArrayList<>();

    @Inject
    public ContactPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {


        getMvpInteractor().getListFriend(new ContactMvpInteractor.ListFriendReady() {
            @Override
            public void listFriend(User item) {
                list.add(item);
            }
        });

        getMvpView().updateContact(list);

    }

}
