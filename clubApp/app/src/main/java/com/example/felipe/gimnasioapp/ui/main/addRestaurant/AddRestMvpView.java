package com.example.felipe.gimnasioapp.ui.main.addRestaurant;

import com.example.felipe.gimnasioapp.data.model.ItemFilter;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import java.util.List;

public interface AddRestMvpView extends MvpView {

    void updateCategory(List<ItemFilter> list);

    void showCategoryDialog();

    void showImageDialog();

    void openMapActivity();

    void showNameError();

    void showMenuCategoryError();

    void showLocationError();

    void showScheduleError();

    void showPhoneError();

    void showBitmapIconImage();

    void showUriIconImage();

    void showBitmapPlaceholderImage();

    void showUriPlaceholder();

    void setVisbilityIcon();
}
