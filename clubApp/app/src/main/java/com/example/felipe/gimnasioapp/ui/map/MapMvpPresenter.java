package com.example.felipe.gimnasioapp.ui.map;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface MapMvpPresenter<V extends MapMvpView, I extends MapMvpInteractor>
        extends MvpPresenter<V, I> {

    void onViewPrepared();

    void onAcceptClick();

    void onLocationReady();
}
