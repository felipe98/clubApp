package com.example.felipe.gimnasioapp.ui.detail.food;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.Commentary;
import com.example.felipe.gimnasioapp.data.model.Food;
import com.example.felipe.gimnasioapp.data.model.ItemFood;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.detail.rate.RateAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder> implements Filterable {

    private List<Food> list;
    private List<Food> tempList;

    public FoodAdapter(List<Food> list) {
        this.list = list;
    }

    public void addItems(List<Food> newList) {
        list.addAll(newList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_food, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    list = tempList;
                } else {

                    ArrayList<Food> filteredList = new ArrayList<>();

                    list = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = list;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //list = (List<Restaurant>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;

        @BindView(R.id.title)
        TextView title;

        private FoodItemAdapter adapter;
        private LinearLayoutManager layoutManager;
        private boolean next;

        ViewHolder(View itemView) {
            super(itemView);
            layoutManager = new LinearLayoutManager(itemView.getContext());
            ButterKnife.bind(this, itemView);
            adapter = new FoodItemAdapter(new ArrayList<ItemFood>());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            next = false;
        }

        void bind(final Food itemModel) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    adapter.addItems(itemModel.getList());

                    if (!next) {
                        title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up, 0);
                        recyclerView.setVisibility(View.VISIBLE);
                        next = true;
                    } else {
                        title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
                        recyclerView.setVisibility(View.GONE);
                        next = false;
                    }

                }
            });


        }
    }
}
