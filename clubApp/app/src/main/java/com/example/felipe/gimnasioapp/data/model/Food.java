package com.example.felipe.gimnasioapp.data.model;

import java.util.List;

import io.realm.RealmObject;

public class Food{

    private String title;
    private List<ItemFood> list;

    public Food() {
    }

    public Food(String title, List<ItemFood> list) {
        this.title = title;
        this.list = list;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ItemFood> getList() {
        return list;
    }

    public void setList(List<ItemFood> list) {
        this.list = list;
    }
}
