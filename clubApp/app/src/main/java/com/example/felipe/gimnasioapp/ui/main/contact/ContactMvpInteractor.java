package com.example.felipe.gimnasioapp.ui.main.contact;

import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.MvpInteractor;

public interface ContactMvpInteractor extends MvpInteractor {

    interface ListFriendReady {

        void listFriend(User item);

    }

    void getListFriend(ListFriendReady listener);
}
