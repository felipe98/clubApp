package com.example.felipe.gimnasioapp.ui.main.address;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;

public class AddressFragment extends BaseFragment implements AddressMvpView {



    public AddressFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_address, container, false);


        return rootView;
    }

    @Override
    protected void setUp(View view) {

    }
}
