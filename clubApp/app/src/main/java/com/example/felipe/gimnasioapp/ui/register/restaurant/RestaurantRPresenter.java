package com.example.felipe.gimnasioapp.ui.register.restaurant;

import android.text.TextUtils;

import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;
import com.example.felipe.gimnasioapp.utils.CommonUtils;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by Felipe on 19/07/2018.
 */

public class RestaurantRPresenter<V extends RestaurantRMvpView, I extends RestaurantRMvpInteractor>
        extends BasePresenter<V, I> implements RestaurantRMvpPresenter<V, I> {

    private Realm realm;

    @Inject
    public RestaurantRPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onRegisterClick(User user, String rePassword) {

        realm = Realm.getDefaultInstance();

        if (TextUtils.isEmpty(user.getName())) {
            getMvpView().showNameEmptyError();
            return;
        }

        if (TextUtils.isEmpty(user.getLastName())) {
            getMvpView().showLastNameEmptyError();
            return;
        }

        if (TextUtils.isEmpty(user.getPostalCode())) {
            getMvpView().showPostalCodeEmptyError();
            return;
        }

        if (TextUtils.isEmpty(user.getEmail())) {
            getMvpView().showEmailEmptyError();
            return;
        }

        if (TextUtils.isEmpty(user.getPass())) {
            getMvpView().showPasswordEmptyError();
            return;
        }

        if (TextUtils.isEmpty(rePassword)) {
            getMvpView().showRePasswordEmptyError();
            return;
        }


        if (!user.getPass().equals(rePassword)) {
            getMvpView().showDifferentRePasswordError();
            return;
        }

        if (!CommonUtils.isEmailValid(user.getEmail())) {
            getMvpView().showFormatEmailError();
            return;
        }

        getMvpView().showLoading();

        getMvpInteractor().registerUser(user, new RestaurantRMvpInteractor.FirebaseListener() {
            @Override
            public void newUser(boolean user) {

                if (!isViewAttached()) return;

                if (user) {
                    getMvpView().openMainActivity();
                    getMvpView().hideLoading();
                    addTypeUser(new TypeUser("RESTAURANT"));
                }
            }
        });
    }

    private void addTypeUser(TypeUser item) {
        realm.beginTransaction();
        realm.copyToRealm(item);
        realm.commitTransaction();
    }
}
