package com.example.felipe.gimnasioapp.ui.main.favorite;

import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import java.util.List;

public interface FavoriteMvpView extends MvpView {

    void updateFavorite(List<Restaurant> list);

}
