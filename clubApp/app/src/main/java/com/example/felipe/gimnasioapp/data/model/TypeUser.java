package com.example.felipe.gimnasioapp.data.model;

import java.io.Serializable;

import io.realm.RealmObject;

public class TypeUser extends RealmObject implements Serializable {

    private String type;

    public TypeUser() {
    }

    public TypeUser(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
