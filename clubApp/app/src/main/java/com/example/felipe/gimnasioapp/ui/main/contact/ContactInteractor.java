package com.example.felipe.gimnasioapp.ui.main.contact;

import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.BaseInteractor;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;

public class ContactInteractor extends BaseInteractor implements ContactMvpInteractor {

    private FirebaseDatabase database;
    private DatabaseReference reference;

    @Inject
    public ContactInteractor(){

    }

    @Override
    public void getListFriend(final ListFriendReady listener) {

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("user");

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User item = dataSnapshot.getValue(User.class);
                listener.listFriend(item);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
