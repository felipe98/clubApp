package com.example.felipe.gimnasioapp.ui.main.addRestaurant;

import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;

import com.example.felipe.gimnasioapp.data.model.ItemFilter;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AddRestPresenter<V extends AddRestMvpView, I extends AddRestMvpInteractor> extends
        BasePresenter<V, I> implements AddRestMvpPresenter<V, I> {

    private List<ItemFilter> list = new ArrayList<>();
    private boolean type;

    @Inject
    public AddRestPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {

        for (int i = 1; i < 5; i++) {
            if (i == 1) {
                list.add(new ItemFilter("AB", false));
            } else if (i == 2) {
                list.add(new ItemFilter("BC", false));
            } else {
                list.add(new ItemFilter("JOP", false));
            }

        }

        getMvpView().updateCategory(list);

    }

    @Override
    public void onMapClick() {
        getMvpView().openMapActivity();
    }

    @Override
    public void onCategoryClick() {
        getMvpView().showCategoryDialog();
    }

    @Override
    public void onAddClick(Restaurant item) {

        if (TextUtils.isEmpty(item.getName())) {
            getMvpView().showNameError();
            return;
        }

        if (TextUtils.isEmpty(item.getCategory())) {
            getMvpView().showMenuCategoryError();
            return;
        }

        if (TextUtils.isEmpty(item.getPhone())) {
            getMvpView().showPhoneError();
            return;
        }

        if (TextUtils.isEmpty(item.getAddress())) {
            getMvpView().showLocationError();
            return;
        }


    }

    @Override
    public void onPlaceholderClick() {
        getMvpView().showImageDialog();
        type = true;
    }

    @Override
    public void onIconClick() {
        getMvpView().showImageDialog();
        type = false;
    }

    @Override
    public void onBitmapResult() {
        if (type){
            getMvpView().showBitmapPlaceholderImage();
        }else {
            getMvpView().showBitmapIconImage();
            getMvpView().setVisbilityIcon();
        }
    }

    @Override
    public void onUriResult() {
        if (type){
            getMvpView().showUriPlaceholder();
        }else {
            getMvpView().showUriIconImage();
            getMvpView().setVisbilityIcon();
        }
    }

}
