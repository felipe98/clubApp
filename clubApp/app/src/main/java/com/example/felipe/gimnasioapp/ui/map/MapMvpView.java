package com.example.felipe.gimnasioapp.ui.map;

import com.example.felipe.gimnasioapp.ui.base.MvpView;

public interface MapMvpView extends MvpView {

    void openMainActivity();

}
