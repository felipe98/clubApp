package com.example.felipe.gimnasioapp.ui.detail.food;

import com.example.felipe.gimnasioapp.data.model.Food;
import com.example.felipe.gimnasioapp.data.model.ItemFood;
import com.example.felipe.gimnasioapp.data.model.ItemGym;
import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class FoodPresenter<V extends FoodMvpView, I extends FoodMvpInteractor>
        extends BasePresenter<V, I> implements FoodMvpPresenter<V, I> {

    List<Food> list = new ArrayList<>();
    List<ItemFood> list1 = new ArrayList<>();

    @Inject
    public FoodPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {
        for (int i = 1; i < 10; i++) list1.add(new ItemFood());
        for (int i = 1; i < 10; i++) list.add(new Food("", list1));
        getMvpView().updateFood(list);
    }
}
