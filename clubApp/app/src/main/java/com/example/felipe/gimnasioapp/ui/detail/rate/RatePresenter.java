package com.example.felipe.gimnasioapp.ui.detail.rate;

import com.example.felipe.gimnasioapp.data.model.Commentary;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RatePresenter<V extends RateMvpView, I extends RateMvpInteractor>
        extends BasePresenter<V, I> implements RateMvpPresenter<V, I> {

    List<Commentary> list = new ArrayList<>();

    @Inject
    public RatePresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {
        for (int i = 1; i < 10; i++) list.add(new Commentary());
        getMvpView().updateRate(list);
    }
}
