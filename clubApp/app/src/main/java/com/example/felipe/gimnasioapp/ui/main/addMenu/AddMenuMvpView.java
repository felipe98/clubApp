package com.example.felipe.gimnasioapp.ui.main.addMenu;

import com.example.felipe.gimnasioapp.ui.base.MvpView;

public interface AddMenuMvpView extends MvpView {

    void showMenuCategoryError();

    void showMenuError();

    void showDetailError();

    void showMoneyError();

}
