package com.example.felipe.gimnasioapp.ui.map;

import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class MapPresenter<V extends MapMvpView, I extends MapMvpInteractor>
        extends BasePresenter<V, I> implements MapMvpPresenter<V, I> {

    @Inject
    public MapPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {
        getMvpView().showLoading();
    }

    @Override
    public void onAcceptClick() {
        getMvpView().openMainActivity();
    }


    @Override
    public void onLocationReady() {
        getMvpView().hideLoading();
    }

}
