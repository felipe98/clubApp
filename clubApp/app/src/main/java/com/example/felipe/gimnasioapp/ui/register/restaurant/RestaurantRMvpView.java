package com.example.felipe.gimnasioapp.ui.register.restaurant;

import com.example.felipe.gimnasioapp.ui.base.MvpView;

/**
 * Created by Felipe on 19/07/2018.
 */

public interface RestaurantRMvpView extends MvpView {

    void openMainActivity();

    void showDifferentRePasswordError();

    void showFormatEmailError();

    void showNameEmptyError();

    void showLastNameEmptyError();

    void showPostalCodeEmptyError();

    void showEmailEmptyError();

    void showPasswordEmptyError();

    void showRePasswordEmptyError();
}
