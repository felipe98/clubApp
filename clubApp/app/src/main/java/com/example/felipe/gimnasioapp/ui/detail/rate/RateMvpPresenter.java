package com.example.felipe.gimnasioapp.ui.detail.rate;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface RateMvpPresenter<V extends RateMvpView, I extends RateMvpInteractor>
        extends MvpPresenter<V, I> {

    void onViewPrepared();
}
