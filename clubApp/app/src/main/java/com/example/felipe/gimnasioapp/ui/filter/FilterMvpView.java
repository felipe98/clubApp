package com.example.felipe.gimnasioapp.ui.filter;

import com.example.felipe.gimnasioapp.data.model.ItemFilter;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import java.util.List;

public interface FilterMvpView extends MvpView {

    void openMainActivity();

    void clearFilter();

    void updateFilter(List<ItemFilter> list);

}
