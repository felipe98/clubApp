package com.example.felipe.gimnasioapp.ui.main.contact;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface ContactMvpPresenter<V extends ContactMvpView, I extends ContactMvpInteractor>
        extends MvpPresenter<V, I> {

    void onViewPrepared();

}
