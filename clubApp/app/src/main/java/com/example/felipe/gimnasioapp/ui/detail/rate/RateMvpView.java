package com.example.felipe.gimnasioapp.ui.detail.rate;

import com.example.felipe.gimnasioapp.data.model.Commentary;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import java.util.List;

public interface RateMvpView extends MvpView {

    void updateRate(List<Commentary> list);
}
