package com.example.felipe.gimnasioapp.ui.detail.food;

import com.example.felipe.gimnasioapp.data.model.Food;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import java.util.List;

public interface FoodMvpView extends MvpView {

    void updateFood(List<Food> list);

}
