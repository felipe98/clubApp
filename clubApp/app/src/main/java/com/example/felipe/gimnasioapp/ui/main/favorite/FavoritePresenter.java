package com.example.felipe.gimnasioapp.ui.main.favorite;

import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

public class FavoritePresenter<V extends FavoriteMvpView, I extends FavoriteMvpInteractor>
        extends BasePresenter<V, I> implements FavoriteMvpPresenter<V, I> {

    private Realm realm;
    private RealmResults<Restaurant> results;

    @Inject
    public FavoritePresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {
        realm = Realm.getDefaultInstance();
        results = realm.where(Restaurant.class).findAll();

    }
}
