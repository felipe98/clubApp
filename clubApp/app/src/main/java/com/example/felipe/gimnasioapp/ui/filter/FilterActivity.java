package com.example.felipe.gimnasioapp.ui.filter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.ItemFilter;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.BaseActivity;
import com.example.felipe.gimnasioapp.ui.detail.DetailActivity;
import com.example.felipe.gimnasioapp.ui.main.MainActivity;
import com.example.felipe.gimnasioapp.ui.main.MainMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.MainMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.MainMvpView;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class FilterActivity extends BaseActivity implements FilterMvpView {

    @Inject
    FilterMvpPresenter<FilterMvpView, FilterMvpInteractor> mPresenter;

    @Inject
    FilterAdapter adapter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    LinearLayoutManager layoutManager;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, FilterActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();


    }

    @OnClick(R.id.btnFilter)
    public void onFilterClick() {
        mPresenter.onFilterClick();
    }

    private void toolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Filtro");
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        toolbar();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.onViewPrepared();

        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.restore, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.restore:
                mPresenter.onRestoreClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("item", "");
        startActivity(intent);
    }

    @Override
    public void clearFilter() {
        adapter.clearCheck();
    }

    @Override
    public void updateFilter(List<ItemFilter> list) {
        adapter.addItems(list);
    }
}
