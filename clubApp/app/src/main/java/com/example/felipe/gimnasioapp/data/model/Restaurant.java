package com.example.felipe.gimnasioapp.data.model;

import java.io.Serializable;

import io.realm.RealmObject;

public class Restaurant extends RealmObject implements Serializable{

    private String name;
    private String category;
    private String address;
    private String phone;
    private String logo;
    private String type;
    private Schedule schedule;
    private boolean state;

    public Restaurant() {
    }

    public Restaurant(String name, String category, String address, String phone, String logo, Schedule schedule, boolean state) {
        this.name = name;
        this.category = category;
        this.address = address;
        this.phone = phone;
        this.logo = logo;
        this.schedule = schedule;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
