package com.example.felipe.gimnasioapp.ui.main.about;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.di.component.ActivityComponent;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutFragment extends BaseFragment implements AboutMvpView {

    @Inject
    AboutMvpPresenter<AboutMvpView, AboutMvpInteractor> mPresenter;

    @BindView(R.id.list)
    ListView list;

    private String[] titleId;
    private String[] subtitleId;

    public AboutFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);

        ActivityComponent component = getActivityComponent();

        if (component != null) {

            component.inject(this);

            setUnBinder(ButterKnife.bind(this, rootView));

            mPresenter.onAttach(this);
        }

        return rootView;
    }

    @Override
    protected void setUp(View view) {

        titleId = getResources().getStringArray(R.array.title);
        subtitleId = getResources().getStringArray(R.array.subtitle);

        AboutAdapter adapter = new AboutAdapter(getActivity(), titleId, subtitleId);

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 6) {
                    final String appName = view.getContext().getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.shaperdroid.NewsAppDemoMaterialDesign")));
                    }
                }
            }
        });

    }
}
