package com.example.felipe.gimnasioapp.ui.main.about;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface AboutMvpPresenter<V extends AboutMvpView, I extends AboutMvpInteractor>
    extends MvpPresenter<V,I>{

    void onViewPrepared();

}
