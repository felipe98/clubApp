package com.example.felipe.gimnasioapp.data.model;

public class ItemFood{

    private String category;
    private String detail;
    private String name;
    private String price;

    public ItemFood(){
    }

    public ItemFood(String category, String detail, String name, String price) {
        this.category = category;
        this.detail = detail;
        this.name = name;
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
