package com.example.felipe.gimnasioapp.ui.detail;

import android.support.design.widget.TabLayout;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.ui.base.MvpView;

public interface DetailMvpView extends MvpView {

    void showStarFavorite();

    void showOutlineFavorite();

    void hideCustomDialog();

    void showCustomDialog();

    void rotateButton(float value);

    void setStartTab(TabLayout.Tab tab, String title, int drawable);

    void setOutTab(TabLayout.Tab tab, String title, int drawable);
}
