package com.example.felipe.gimnasioapp.ui.main.addMenu;

import android.text.TextUtils;

import com.example.felipe.gimnasioapp.data.model.ItemFood;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

import javax.inject.Inject;

public class AddMenuPresenter<V extends AddMenuMvpView, I extends AddMenuMvpInteractor>
        extends BasePresenter<V, I> implements AddMenuMvpPresenter<V,I>{

    @Inject
    public AddMenuPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {

    }

    @Override
    public void onAddClick(ItemFood item) {

        if (TextUtils.isEmpty(item.getCategory())) {
            getMvpView().showMenuCategoryError();
            return;
        }

        if (TextUtils.isEmpty(item.getName())) {
            getMvpView().showMenuError();
            return;
        }

        if (TextUtils.isEmpty(item.getDetail())) {
            getMvpView().showDetailError();
            return;
        }

        if (TextUtils.isEmpty(item.getPrice())) {
            getMvpView().showMoneyError();
            return;
        }

    }

    @Override
    public void onMenuClick() {

    }
}
