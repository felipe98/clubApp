package com.example.felipe.gimnasioapp.ui.main.addMenu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.ItemFood;
import com.example.felipe.gimnasioapp.di.component.ActivityComponent;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestMvpView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMenuFragment extends BaseFragment implements AddMenuMvpView {

    @Inject
    AddMenuMvpPresenter<AddMenuMvpView, AddMenuMvpInteractor> mPresenter;

    @BindView(R.id.iLMenu)
    TextInputLayout iLMenu;
    @BindView(R.id.inputMenu)
    EditText editMenu;

    @BindView(R.id.iLFood)
    TextInputLayout iLFood;
    @BindView(R.id.inputFood)
    EditText editFood;

    @BindView(R.id.iLDetail)
    TextInputLayout iLDetail;
    @BindView(R.id.inputDetail)
    EditText editDetail;

    @BindView(R.id.iLPrice)
    TextInputLayout iLPrice;
    @BindView(R.id.inputPrice)
    EditText ediPrice;


    public AddMenuFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_menu, container, false);

        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, rootView));
            mPresenter.onAttach(this);
        }

        return rootView;
    }

    @OnClick(R.id.btnMenu)
    public void onMenuClick() {
        mPresenter.onMenuClick();
    }

    @OnClick(R.id.btnAdd)
    public void onAddClick() {
        mPresenter.onAddClick(new ItemFood(editMenu.getText().toString(),editDetail.getText().toString(),
                editFood.getText().toString(),ediPrice.getText().toString()));
    }

    @Override
    protected void setUp(View view) {

        mPresenter.onViewPrepared();

    }

    @Override
    public void showMenuCategoryError() {
        iLMenu.setError("Campo vacío");
    }

    @Override
    public void showMenuError() {
        iLFood.setError("Campo vacío");
    }

    @Override
    public void showDetailError() {
        iLDetail.setError("Campo vacío");
    }

    @Override
    public void showMoneyError() {
        iLPrice.setError("Campo vacío");
    }
}
