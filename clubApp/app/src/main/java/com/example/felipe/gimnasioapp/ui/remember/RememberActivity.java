package com.example.felipe.gimnasioapp.ui.remember;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.ui.base.BaseActivity;
import com.example.felipe.gimnasioapp.ui.login.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RememberActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, RememberActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remember);

        setUnBinder(ButterKnife.bind(this));

        setUp();

    }

    @Override
    protected void setUp() {
        toolbar();
    }

    private void toolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Recuperar contraseña");
        }
    }

    @Override
    protected void onDestroy() {
        //mPresenter.onDetach();
        super.onDestroy();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
