package com.example.felipe.gimnasioapp.ui.register.restaurant;

import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

/**
 * Created by Felipe on 19/07/2018.
 */

public interface RestaurantRMvpPresenter<V extends RestaurantRMvpView,I extends RestaurantRMvpInteractor>
        extends MvpPresenter<V,I>{

    void onRegisterClick(User user, String rePassword);


}
