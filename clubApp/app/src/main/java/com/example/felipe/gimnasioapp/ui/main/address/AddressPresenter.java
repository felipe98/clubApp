package com.example.felipe.gimnasioapp.ui.main.address;

import com.example.felipe.gimnasioapp.ui.base.BasePresenter;
import com.example.felipe.gimnasioapp.ui.main.addRestaurant.AddRestPresenter;

import javax.inject.Inject;

public class AddressPresenter<V extends AddressMvpView, I extends AddressMvpInteractor>
        extends BasePresenter<V, I> implements AddressMvpPresenter<V, I> {


    @Inject
    public AddressPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

}
