package com.example.felipe.gimnasioapp.ui.main.addMenu;

import com.example.felipe.gimnasioapp.data.model.ItemFood;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface AddMenuMvpPresenter<V extends AddMenuMvpView, I extends AddMenuMvpInteractor>
        extends MvpPresenter<V, I> {


    void onViewPrepared();

    void onAddClick(ItemFood item);

    void onMenuClick();

}
