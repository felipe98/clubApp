package com.example.felipe.gimnasioapp.ui.main.addRestaurant;

import android.graphics.Bitmap;
import android.net.Uri;

import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface AddRestMvpPresenter<V extends AddRestMvpView,I extends AddRestMvpInteractor>
        extends MvpPresenter<V,I> {


    void onViewPrepared();

    void onMapClick();

    void onCategoryClick();

    void onAddClick(Restaurant item);

    void onPlaceholderClick();

    void onIconClick();

    void onBitmapResult();

    void onUriResult();
}
