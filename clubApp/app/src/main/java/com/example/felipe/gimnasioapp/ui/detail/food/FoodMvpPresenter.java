package com.example.felipe.gimnasioapp.ui.detail.food;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface FoodMvpPresenter<V extends FoodMvpView, I extends FoodMvpInteractor>
        extends MvpPresenter<V, I> {

    void onViewPrepared();
}
