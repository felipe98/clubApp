package com.example.felipe.gimnasioapp.data.model;

import io.realm.RealmObject;

public class ItemFilter extends RealmObject {

    private String title;
    private boolean check;

    public ItemFilter(String title, boolean check) {
        this.title = title;
        this.check = check;
    }

    public ItemFilter() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
