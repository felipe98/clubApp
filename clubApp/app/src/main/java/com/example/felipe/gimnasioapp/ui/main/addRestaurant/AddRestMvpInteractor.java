package com.example.felipe.gimnasioapp.ui.main.addRestaurant;

import com.example.felipe.gimnasioapp.data.model.ItemFilter;
import com.example.felipe.gimnasioapp.ui.base.MvpInteractor;

import java.util.List;

public interface AddRestMvpInteractor extends MvpInteractor {

    List<ItemFilter> getDataFromServer();

}
