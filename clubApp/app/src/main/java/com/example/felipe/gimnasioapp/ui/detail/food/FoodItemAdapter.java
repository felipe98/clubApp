package com.example.felipe.gimnasioapp.ui.detail.food;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.Food;
import com.example.felipe.gimnasioapp.data.model.ItemFood;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FoodItemAdapter extends RecyclerView.Adapter<FoodItemAdapter.ViewHolder> {

    private List<ItemFood> list;

    public FoodItemAdapter(List<ItemFood> list) {
        this.list = list;
    }

    public void addItems(List<ItemFood> newList) {
        list.addAll(newList);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_food_sale, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.add)
        CardView add;

        @BindView(R.id.subtract)
        CardView subtract;

        @BindView(R.id.count)
        TextView count;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final ItemFood itemModel) {


            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subtract.setVisibility(View.VISIBLE);
                    count.setVisibility(View.VISIBLE);
                }
            });

            subtract.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subtract.setVisibility(View.GONE);
                    count.setVisibility(View.GONE);
                }
            });
        }

    }
}
