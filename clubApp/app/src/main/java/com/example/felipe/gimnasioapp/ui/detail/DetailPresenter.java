package com.example.felipe.gimnasioapp.ui.detail;

import android.support.design.widget.TabLayout;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

public class DetailPresenter<V extends DetailMvpView, I extends DetailMvpInteractor>
        extends BasePresenter<V, I> implements DetailMvpPresenter<V, I> {

    private Realm realm;
    private RealmResults<Restaurant> results;

    @Inject
    public DetailPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    private String setTextTitle(int pos) {
        if (pos == 0) return "Menú";
        else return "Comentarios";
    }

    @Override
    public void onCreateOptionsMenu(boolean fav) {
        if (fav) getMvpView().showOutlineFavorite();
        else getMvpView().showStarFavorite();
    }

    @Override
    public void onFavoriteClick(boolean fav, Restaurant item) {
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        if (fav) {
            realm.copyToRealm(item);
            getMvpView().showStarFavorite();
        } else {
            RealmResults<Restaurant> rows = realm.where(Restaurant.class).equalTo("name", item.getName()).findAll();
            rows.deleteAllFromRealm();
            getMvpView().showOutlineFavorite();
        }
        realm.commitTransaction();
    }

    @Override
    public void onCustomDialogClosed() {
        getMvpView().rotateButton(1);
        getMvpView().hideCustomDialog();
    }

    @Override
    public void onCustomDialogOpened() {
        getMvpView().rotateButton(135f);
        getMvpView().showCustomDialog();

    }

    @Override
    public void onPageSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0)
            getMvpView().setStartTab(tab, setTextTitle(tab.getPosition()), R.drawable.menu_star);
        else
            getMvpView().setStartTab(tab, setTextTitle(tab.getPosition()), R.drawable.ic_comment_star);
    }

    @Override
    public void onPageUnselected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0)
            getMvpView().setOutTab(tab, setTextTitle(tab.getPosition()), R.drawable.menu_out);
        else
            getMvpView().setOutTab(tab, setTextTitle(tab.getPosition()), R.drawable.ic_comment_out);
    }

}
