package com.example.felipe.gimnasioapp.ui.main.order;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface OrderMvpPresenter<V extends OrderMvpView, I extends OrderMvpInteractor> extends MvpPresenter<V,I> {
}
