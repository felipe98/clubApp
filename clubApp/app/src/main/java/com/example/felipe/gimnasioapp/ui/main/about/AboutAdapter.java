package com.example.felipe.gimnasioapp.ui.main.about;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.R;

public class AboutAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] titleId;
    private final String[] subtitleId;

    public void addItems(){

    }

    public AboutAdapter(Activity context, String[] titleId, String[] subtitleId) {
        super(context, R.layout.lsv_item_about, titleId);
        this.context = context;
        this.titleId = titleId;
        this.subtitleId = subtitleId;
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"InflateParams", "ViewHolder"}) View rowView = inflater.inflate(R.layout.lsv_item_about, null, true);

        TextView title = rowView.findViewById(R.id.title);
        TextView subtitle = rowView.findViewById(R.id.subtitle);

        title.setText(titleId[position]);
        subtitle.setText(subtitleId[position]);

        return rowView;
    }
}