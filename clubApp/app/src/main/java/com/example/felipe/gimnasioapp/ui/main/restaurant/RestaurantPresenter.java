package com.example.felipe.gimnasioapp.ui.main.restaurant;

import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Felipe on 17/07/2018.
 */

public class RestaurantPresenter<V extends RestaurantMvpView, I extends RestaurantMvpInteractor>
        extends BasePresenter<V, I> implements RestaurantMvpPresenter<V, I> {

    List<Restaurant> list = new ArrayList<>();

    @Inject
    public RestaurantPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {
        for (int i = 1; i < 5; i++) list.add(new Restaurant());
        getMvpView().updateClub(list);
    }

    @Override
    public void onFilterClick() {
        getMvpView().openFilterActivity();
    }

    @Override
    public void onRefreshCalled() {

    }
}
