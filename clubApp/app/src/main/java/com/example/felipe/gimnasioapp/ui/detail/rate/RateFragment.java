package com.example.felipe.gimnasioapp.ui.detail.rate;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.Commentary;
import com.example.felipe.gimnasioapp.di.component.ActivityComponent;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodAdapter;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodMvpInteractor;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodMvpPresenter;
import com.example.felipe.gimnasioapp.ui.detail.food.FoodMvpView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RateFragment extends BaseFragment implements RateMvpView {

    @Inject
    RateMvpPresenter<RateMvpView, RateMvpInteractor> mPresenter;

    @Inject
    RateAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public RateFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rate, container, false);
        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, rootView));
            mPresenter.onAttach(this);
            //mBlogAdapter.setCallback(this);
        }

        return rootView;
    }

    @Override
    protected void setUp(View view) {
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.onViewPrepared();

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateRate(List<Commentary> list) {
        adapter.addItems(list);
    }
}
