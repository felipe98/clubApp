package com.example.felipe.gimnasioapp.ui.detail;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.BaseActivity;
import com.example.felipe.gimnasioapp.ui.base.MvpView;
import com.example.felipe.gimnasioapp.ui.register.RegisterMvpInteractor;
import com.example.felipe.gimnasioapp.ui.register.RegisterMvpPresenter;
import com.example.felipe.gimnasioapp.ui.register.RegisterMvpView;
import com.example.felipe.gimnasioapp.ui.register.RegisterPagerAdapter;

import org.w3c.dom.Text;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class DetailActivity extends BaseActivity implements DetailMvpView {

    @Inject
    DetailMvpPresenter<DetailMvpView, DetailMvpInteractor> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.more)
    FloatingActionButton btnMore;

    private TextView text_star;
    private TextView text_out;
    private int height;
    private AlertDialog dialog;

    private Restaurant item;

    private Menu menu;
    private Realm realm;
    private RealmResults<Restaurant> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        item = getObject();

        setUp();

    }

    private Restaurant getObject() {
        return (Restaurant) getIntent().getExtras().get("item");
    }

    @OnClick(R.id.more)
    public void onMoreDetailClick() {
        mPresenter.onCustomDialogOpened();
    }

    private TextView inflateText() {
        return (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
    }


    private int getStatusBarHeight() {
        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier("status_bar_height",
                "dimen", "android");
        if (idStatusBarHeight > 0) height = getResources().getDimensionPixelSize(idStatusBarHeight);
        else height = 0;
        return height;
    }

    @Override
    protected void setUp() {
        realm = Realm.getDefaultInstance();
        results = realm.where(Restaurant.class).equalTo("name", item.getName()).findAll();

        toolbar();
        text_star = inflateText();
        text_out = inflateText();
        mViewPager.setAdapter(new DetailPageAdapter(getSupportFragmentManager()));
        viewPagerListener();
        tabLayout.setupWithViewPager(mViewPager);
        mPresenter.onPageUnselected(tabLayout.getTabAt(1));
    }

    private void viewPagerListener() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mPresenter.onPageSelected(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                mPresenter.onPageUnselected(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void toolbar() {
        setSupportActionBar(toolbar);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favorite, menu);
        super.onCreateOptionsMenu(menu);
        this.menu = menu;
        mPresenter.onCreateOptionsMenu(results.isEmpty());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_fav:
                mPresenter.onFavoriteClick(results.isEmpty(), item);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showStarFavorite() {
        menu.getItem(0).setIcon(R.drawable.ic_fav_red);
    }

    @Override
    public void showOutlineFavorite() {
        menu.getItem(0).setIcon(R.drawable.ic_star_outline);
    }

    @Override
    public void hideCustomDialog() {
        dialog.dismiss();
    }

    @Override
    public void showCustomDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Menu item");
        View vistaAlert = LayoutInflater.from(this).inflate(R.layout.custom_dialog_more, null);
        builder.setView(vistaAlert);

        builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPresenter.onCustomDialogClosed();

            }
        });

        dialog = builder.create();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mPresenter.onCustomDialogClosed();
            }
        });

        dialog.show();
    }

    @Override
    public void rotateButton(float value) {
        final OvershootInterpolator interpolator = new OvershootInterpolator();
        ViewCompat.animate(btnMore).
                rotation(value).
                withLayer().
                setDuration(300).
                setInterpolator(interpolator).
                start();
    }

    @Override
    public void setStartTab(TabLayout.Tab tab, String title, int drawable) {
        text_star.setText(title);
        text_star.setTextColor(getResources().getColor(R.color.tab_text_start));
        text_star.setCompoundDrawablesWithIntrinsicBounds(0, drawable, 0, 0);
        tab.setCustomView(text_star);
    }

    @Override
    public void setOutTab(TabLayout.Tab tab, String title, int drawable) {
        text_out.setText(title);
        text_out.setTextColor(getResources().getColor(R.color.tab_text_out));
        text_out.setCompoundDrawablesWithIntrinsicBounds(0, drawable, 0, 0);
        tab.setCustomView(text_out);
    }
}
