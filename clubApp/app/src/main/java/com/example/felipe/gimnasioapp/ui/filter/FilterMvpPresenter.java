package com.example.felipe.gimnasioapp.ui.filter;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface FilterMvpPresenter<V extends FilterMvpView, I extends FilterMvpInteractor> extends MvpPresenter<V,I> {

    void onViewPrepared();

    void onRestoreClick();

    void onFilterClick();

}
