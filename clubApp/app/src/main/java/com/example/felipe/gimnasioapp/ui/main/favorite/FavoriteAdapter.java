package com.example.felipe.gimnasioapp.ui.main.favorite;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.ItemGym;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.detail.DetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {

    private List<Restaurant> item;
    private OnItemClickListener listener;

    public FavoriteAdapter(List<Restaurant> arrayList) {
        this.item = arrayList;
    }

    public void addItems(List<Restaurant> list) {
        this.item = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_restaurant, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(item.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.image)
        ImageView image;

        @Nullable
        @BindView(R.id.text)
        TextView title;

        private Realm realm;
        private RealmResults<Restaurant> results;

        ViewHolder(View itemView) {
            super(itemView);
            //this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            realm = Realm.getDefaultInstance();
        }

        void bind(final Restaurant itemModel, final OnItemClickListener listener) {

            results = realm.where(Restaurant.class).equalTo("name", itemModel.getName()).findAll();

            title.setText(itemModel.getName());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
                    itemView.getContext().startActivity(intent);
                }
            });

            if (image != null) {
                verifyFav(results);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!results.isEmpty()) {
                            removeFavorite(itemModel);
                            notifyDataSetChanged();
                        } else {
                            addFavorite(itemModel);
                        }
                    }
                });
            }


        }
        private void verifyFav(RealmResults<Restaurant> results) {
            if (!results.isEmpty()) image.setImageResource(R.drawable.ic_fav_red);
            else image.setImageResource(R.drawable.ic_fav_out);
        }

        private void addFavorite(Restaurant item) {
            realm.beginTransaction();
            realm.copyToRealm(item);
            realm.commitTransaction();
            image.setImageResource(R.drawable.ic_fav_red);
        }

        private void removeFavorite(Restaurant item) {
            realm.beginTransaction();
            RealmResults<Restaurant> rows = realm.where(Restaurant.class).equalTo("name", item.getName()).findAll();
            rows.deleteAllFromRealm();
            realm.commitTransaction();
            image.setImageResource(R.drawable.ic_fav_out);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ItemGym itemGym, int position);
    }
}

