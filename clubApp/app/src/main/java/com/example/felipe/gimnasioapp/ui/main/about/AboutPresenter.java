package com.example.felipe.gimnasioapp.ui.main.about;

import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class AboutPresenter<V extends AboutMvpView, I  extends AboutMvpInteractor>
    extends BasePresenter<V,I> implements AboutMvpPresenter<V,I>{


    @Inject
    public AboutPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {

    }
}
