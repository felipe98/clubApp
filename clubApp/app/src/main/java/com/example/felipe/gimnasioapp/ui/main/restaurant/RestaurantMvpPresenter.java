package com.example.felipe.gimnasioapp.ui.main.restaurant;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

/**
 * Created by Felipe on 17/07/2018.
 */

public interface RestaurantMvpPresenter<V extends RestaurantMvpView, I extends RestaurantMvpInteractor>
        extends MvpPresenter<V, I> {

    void onViewPrepared();

    void onFilterClick();

    void onRefreshCalled();

}
