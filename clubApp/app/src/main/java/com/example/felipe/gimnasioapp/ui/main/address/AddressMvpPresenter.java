package com.example.felipe.gimnasioapp.ui.main.address;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface AddressMvpPresenter<V extends AddressMvpView, I extends AddressMvpInteractor> extends
        MvpPresenter<V,I>{

}
