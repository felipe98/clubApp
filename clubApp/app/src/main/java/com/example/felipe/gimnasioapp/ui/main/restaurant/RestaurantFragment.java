package com.example.felipe.gimnasioapp.ui.main.restaurant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.ItemGym;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.di.component.ActivityComponent;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;
import com.example.felipe.gimnasioapp.ui.filter.FilterActivity;
import com.example.felipe.gimnasioapp.ui.login.LoginActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestaurantFragment extends BaseFragment implements RestaurantMvpView {

    @Inject
    RestaurantMvpPresenter<RestaurantMvpView, RestaurantMvpInteractor> mPresenter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    RestaurantAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;

    @BindView(R.id.editSearch)
    EditText txtSearch;

    @BindView(R.id.clearText)
    ImageView clear;

    public RestaurantFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recent, container, false);

        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, rootView));
            mPresenter.onAttach(this);
            //mBlogAdapter.setCallback(this);
        }

        return rootView;
    }


    @OnClick(R.id.clearText)
    public void onClearClick() {
        txtSearch.getText().clear();
    }

    @OnClick(R.id.filter)
    public void onFilterClick() {
        mPresenter.onFilterClick();
    }

    public void initSearchView() {
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) clear.setVisibility(View.VISIBLE);
                else clear.setVisibility(View.GONE);

                adapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void openActivityOnTokenExpire() {

    }

    @Override
    protected void setUp(View view) {
        initSearchView();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.onViewPrepared();

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showMessage(int resId) {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void updateClub(List<Restaurant> list) {
        adapter.addItems(list);
    }

    @Override
    public void openFilterActivity() {
        startActivity(FilterActivity.getStartIntent(getActivity()));
    }
}
