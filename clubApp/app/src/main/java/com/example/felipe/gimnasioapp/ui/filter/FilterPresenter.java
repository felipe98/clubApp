package com.example.felipe.gimnasioapp.ui.filter;

import com.example.felipe.gimnasioapp.data.model.ItemFilter;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.BasePresenter;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

public class FilterPresenter<V extends FilterMvpView, I extends FilterMvpInteractor> extends BasePresenter<V, I>
        implements FilterMvpPresenter<V, I> {

    List<ItemFilter> list = new ArrayList<>();

    private Realm realm;

    @Inject
    public FilterPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }

    @Override
    public void onViewPrepared() {
        for (int i = 1; i < 5; i++)
            if (i == 1) {
                list.add(new ItemFilter("AB", false));
            } else if (i == 2) {
                list.add(new ItemFilter("BC", false));
            } else {
                list.add(new ItemFilter("JOP", false));
            }

        getMvpView().updateFilter(list);
    }

    @Override
    public void onRestoreClick() {
        getMvpView().clearFilter();
    }

    @Override
    public void onFilterClick() {
        realm = Realm.getDefaultInstance();

        clearFilter();

        for (ItemFilter item : FilterAdapter.list) {
            if (item.isCheck()) addFilter(item);
        }

        getMvpView().openMainActivity();
    }
    private void clearFilter() {
        realm.beginTransaction();
        RealmResults<ItemFilter> rows = realm.where(ItemFilter.class).findAll();
        rows.deleteAllFromRealm();
        realm.commitTransaction();
    }

    private void addFilter(ItemFilter item) {
        realm.beginTransaction();
        realm.copyToRealm(item);
        realm.commitTransaction();
    }

}
