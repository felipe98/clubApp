package com.example.felipe.gimnasioapp.ui.main.restaurant;

import com.example.felipe.gimnasioapp.data.model.ItemGym;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import java.util.List;

/**
 * Created by Felipe on 17/07/2018.
 */

public interface RestaurantMvpView extends MvpView {

    void updateClub(List<Restaurant> list);

    void openFilterActivity();

}
