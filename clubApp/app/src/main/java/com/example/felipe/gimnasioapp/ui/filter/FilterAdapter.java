package com.example.felipe.gimnasioapp.ui.filter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.ItemFilter;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.main.favorite.FavoriteAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {

    public static List<ItemFilter> list;

    private boolean isCleared;

    public FilterAdapter(List<ItemFilter> arrayList) {
        list = arrayList;
    }

    public void addItems(List<ItemFilter> list) {
        FilterAdapter.list = list;
        notifyDataSetChanged();
    }

    public void clearCheck() {
        for (ItemFilter item : list)
            if (item.isCheck()) item.setCheck(false);
        notifyDataSetChanged();
        isCleared = true;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_filter, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.filter_title)
        TextView title;

        @BindView(R.id.check)
        CheckBox checkBox;

        private Realm realm;
        private RealmResults<ItemFilter> results;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            realm = Realm.getDefaultInstance();
        }

        void bind(final ItemFilter item) {

            title.setText(item.getTitle());

            results = realm.where(ItemFilter.class).equalTo("title", item.getTitle()).findAll();

            if (!isCleared) verifyFav(results, item);
            else checkBox.setChecked(false);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!item.isCheck()) {
                        item.setCheck(true);
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                        item.setCheck(false);
                    }
                }
            });
        }

        private void verifyFav(RealmResults<ItemFilter> results, ItemFilter item) {
            if (!results.isEmpty()) {
                checkBox.setChecked(true);
                item.setCheck(true);
            } else {
                checkBox.setChecked(false);
                item.setCheck(false);
            }
        }
    }

}
