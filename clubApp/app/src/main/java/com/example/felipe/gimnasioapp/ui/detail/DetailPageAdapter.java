package com.example.felipe.gimnasioapp.ui.detail;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.felipe.gimnasioapp.ui.detail.food.FoodFragment;
import com.example.felipe.gimnasioapp.ui.detail.rate.RateFragment;

public class DetailPageAdapter extends FragmentStatePagerAdapter {

    private static int int_items = 2;

    DetailPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FoodFragment();
            case 1:
                return new RateFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return int_items;
    }
}
