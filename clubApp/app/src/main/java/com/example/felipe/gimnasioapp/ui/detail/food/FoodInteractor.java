package com.example.felipe.gimnasioapp.ui.detail.food;

import com.example.felipe.gimnasioapp.ui.base.BaseInteractor;

import javax.inject.Inject;

public class FoodInteractor extends BaseInteractor implements FoodMvpInteractor {

    @Inject
    public FoodInteractor() {
    }
}
