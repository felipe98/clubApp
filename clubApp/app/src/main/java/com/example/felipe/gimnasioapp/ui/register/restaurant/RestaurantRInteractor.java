package com.example.felipe.gimnasioapp.ui.register.restaurant;

import android.support.annotation.NonNull;

import com.example.felipe.gimnasioapp.data.model.TypeUser;
import com.example.felipe.gimnasioapp.data.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by Felipe on 19/07/2018.
 */

public class RestaurantRInteractor implements RestaurantRMvpInteractor {

    private FirebaseDatabase database;
    private DatabaseReference reference;

    private FirebaseAuth mAuth;

    @Inject
    public RestaurantRInteractor() {

    }

    @Override
    public void searchItem() {

    }

    @Override
    public void registerUser(User user, @NonNull final FirebaseListener listener) {


        mAuth = FirebaseAuth.getInstance();

        database = FirebaseDatabase.getInstance();

        reference = database.getReference("user");

        reference.push().setValue(user);

        mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPass())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        listener.newUser(task.isSuccessful());
                    }
                });
    }

}
