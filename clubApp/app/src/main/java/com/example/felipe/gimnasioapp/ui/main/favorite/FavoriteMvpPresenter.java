package com.example.felipe.gimnasioapp.ui.main.favorite;

import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface FavoriteMvpPresenter<V extends FavoriteMvpView, I extends FavoriteMvpInteractor>
        extends MvpPresenter<V, I> {

    void onViewPrepared();
}
