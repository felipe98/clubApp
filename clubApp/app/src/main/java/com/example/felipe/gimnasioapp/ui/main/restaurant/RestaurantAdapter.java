package com.example.felipe.gimnasioapp.ui.main.restaurant;

import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.ItemGym;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.detail.DetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.ViewHolder> implements Filterable {

    private List<Restaurant> list;
    private List<Restaurant> tempList;

    private OnItemClickListener listener;

    public RestaurantAdapter(List<Restaurant> arrayList) {
        this.list = arrayList;
    }

    public void addItems(List<Restaurant> list) {
        this.list = list;
        this.tempList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_restaurant, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    list = tempList;
                } else {

                    ArrayList<Restaurant> filteredList = new ArrayList<>();

                    for (Restaurant item : tempList) {
                        if (item.getName().toLowerCase().contains(charString)) {
                            filteredList.add(item);
                        }
                    }

                    list = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = list;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                list = (List<Restaurant>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.img_fav)
        ImageView image;

        @Nullable
        @BindView(R.id.text)
        TextView title;

        private Realm realm;
        private RealmResults<Restaurant> results;

        ViewHolder(View itemView) {
            super(itemView);
            //this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            realm = Realm.getDefaultInstance();
        }

        void bind(final Restaurant itemModel, final OnItemClickListener listener) {

            itemModel.setName("AB");

            results = realm.where(Restaurant.class).equalTo("name", itemModel.getName()).findAll();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
                    intent.putExtra("item", itemModel);
                    itemView.getContext().startActivity(intent);
                }
            });

            if (image != null) {
                verifyFav(results);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!results.isEmpty()) {
                            removeFavorite(itemModel);
                            notifyDataSetChanged();
                        } else {
                            addFavorite(itemModel);
                        }
                    }
                });
            }


        }

        private void verifyFav(RealmResults<Restaurant> results) {
            if (!results.isEmpty()) image.setImageResource(R.drawable.ic_fav_red);
            else image.setImageResource(R.drawable.ic_fav_out);
        }

        private void addFavorite(Restaurant item) {
            realm.beginTransaction();
            realm.copyToRealm(item);
            realm.commitTransaction();
            image.setImageResource(R.drawable.ic_fav_red);
        }

        private void removeFavorite(Restaurant item) {
            realm.beginTransaction();
            RealmResults<Restaurant> rows = realm.where(Restaurant.class).equalTo("name", item.getName()).findAll();
            rows.deleteAllFromRealm();
            realm.commitTransaction();
            image.setImageResource(R.drawable.ic_fav_out);
        }
    }


    public interface OnItemClickListener {
        void onItemClick();
    }
}

