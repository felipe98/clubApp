package com.example.felipe.gimnasioapp.ui.detail;

import android.support.design.widget.TabLayout;
import android.widget.TextView;

import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.ui.base.MvpPresenter;

public interface DetailMvpPresenter<V extends DetailMvpView, I extends DetailMvpInteractor>
        extends MvpPresenter<V, I> {

    void onCreateOptionsMenu(boolean fav);

    void onFavoriteClick(boolean fav, Restaurant item);

    void onCustomDialogClosed();

    void onCustomDialogOpened();

    void onPageSelected(TabLayout.Tab tab);

    void onPageUnselected(TabLayout.Tab tab);

}
