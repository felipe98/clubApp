package com.example.felipe.gimnasioapp.ui.main.favorite;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.felipe.gimnasioapp.R;
import com.example.felipe.gimnasioapp.data.model.Restaurant;
import com.example.felipe.gimnasioapp.di.component.ActivityComponent;
import com.example.felipe.gimnasioapp.ui.base.BaseFragment;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantAdapter;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantMvpInteractor;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantMvpPresenter;
import com.example.felipe.gimnasioapp.ui.main.restaurant.RestaurantMvpView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class FavoriteFragment extends BaseFragment implements FavoriteMvpView {

    @Inject
    FavoriteMvpPresenter<FavoriteMvpView, FavoriteMvpInteractor> mPresenter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    FavoriteAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;

    private Realm realm;
    private RealmResults<Restaurant> results;

    public FavoriteFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorite, container, false);

        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, rootView));
            mPresenter.onAttach(this);
            //mBlogAdapter.setCallback(this);
        }

        return rootView;
    }

    @Override
    protected void setUp(View view) {
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.onViewPrepared();
        realm = Realm.getDefaultInstance();
        results = realm.where(Restaurant.class).findAll();

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateFavorite(List<Restaurant> list) {
        adapter.addItems(results);
    }
}
