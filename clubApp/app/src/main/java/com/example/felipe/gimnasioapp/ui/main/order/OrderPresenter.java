package com.example.felipe.gimnasioapp.ui.main.order;

import com.example.felipe.gimnasioapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class OrderPresenter<V extends OrderMvpView, I extends OrderMvpInteractor> extends BasePresenter<V, I>
        implements OrderMvpPresenter<V, I> {

    @Inject
    public OrderPresenter(I mMvpInteractor) {
        super(mMvpInteractor);
    }
}
