package com.example.felipe.gimnasioapp.ui.register.restaurant;

import android.support.annotation.NonNull;

import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.MvpInteractor;

/**
 * Created by Felipe on 19/07/2018.
 */

public interface RestaurantRMvpInteractor extends MvpInteractor {

    interface FirebaseListener {

        void newUser(boolean user);

    }

    void registerUser(User user, @NonNull FirebaseListener listener);

}
