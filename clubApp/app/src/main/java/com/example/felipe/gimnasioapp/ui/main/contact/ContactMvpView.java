package com.example.felipe.gimnasioapp.ui.main.contact;

import com.example.felipe.gimnasioapp.data.model.User;
import com.example.felipe.gimnasioapp.ui.base.MvpView;

import java.util.List;

public interface ContactMvpView extends MvpView {

    void updateContact(List<User> list);
}
